#!/bin/sh

if [ "$DATABASE" = "postgres" -a "$SERVICE" = "Django" ]
then
    echo "Waiting for Postgres ...."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      echo "INFO: Waiting for Postgres"
      sleep 0.1
    done
    echo "INFO: Postgres DB started"
    echo "INFO: Running the migrations"
    python manage.py migrate
    echo "INFO: Collecting staticfiles"
    python manage.py collectstatic --no-input --clear  -i 'libs' -i 'css' -i 'js' -v 3
    python manage.py compress --force -v 3
else
    echo "Waiting for Postgres ...."
    while ! nc -z $SQL_HOST $SQL_PORT; do
      echo "INFO: Waiting for Postgres"
      sleep 0.1
    done
    echo "Postgres DB started"
fi
exec "$@"
