# -*- coding: utf-8 -*-

from django.db import models
from meghauth.models import (MeghSvanaUserCoinsModel,)

class CustomerCoinsModelManager(models.Manager):
    """
    managers for cutomer wallet 
    Proxied from --> MeghSvanaUserCoinsModel
    """
    def get_queryset(self):
        return super(CustomerCoinsModelManager, self).get_queryset().filter(user__role='CS')
