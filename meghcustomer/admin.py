from django.contrib import admin
from django.contrib.auth.models  import Group
from meghcustomer.models import CustomerProfile, Order


class CustomerProfileAdmin(admin.ModelAdmin):

    model = CustomerProfile
    fields = ('customer','birth_date','birth_time','birth_place_state','birth_place_country',
              'present_country','present_state','present_city','primary_language','secondary_language', 'created_at', 'updated_at', 'created_by', 'updated_by')
    list_display = ('customer','birth_date','birth_time','birth_place_state','birth_place_country',
              'present_country','present_state','present_city','primary_language','secondary_language', 'created_at', 'updated_at', 'created_by', 'updated_by')
    readonly_fields = ('created_at', 'updated_at', 'created_by', 'updated_by')
class OrderAdmin(admin.ModelAdmin):
    model = Order
    fields = ('oid', 'customer', 'services', 'invoice', 'amount', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by')    
    list_display = ('oid', 'customer', 'services', 'invoice', 'amount', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by')    
    readonly_fields = ('oid', 'created_at', 'updated_at', 'created_by', 'updated_by')
# Register your models here.
admin.site.register(CustomerProfile, CustomerProfileAdmin)
admin.site.register(Order, OrderAdmin)

