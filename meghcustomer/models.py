# -*- coding: utf-8 -*-

from django.db import models
from meghauth.models import (MeghSvanaUser, CustomerUser, MeghSvanaUserCoinsModel)
from meghcustomer.managers import (CustomerCoinsModelManager,)
import uuid

# Create your models here.

class CustomerProfile(models.Model):
    """
    Profile Model for Customer
    """
    customer = models.ForeignKey(CustomerUser, on_delete=models.CASCADE, related_name='customer_profile')
    birth_date = models.DateField()
    birth_time = models.TimeField()
    birth_place_state = models.CharField(max_length=70)
    birth_place_country = models.CharField(max_length=70)
    present_country = models.CharField(max_length=70)
    present_state = models.CharField(max_length=70)
    present_city = models.CharField(max_length=70)
    primary_language = models.CharField(max_length=50)
    secondary_language = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True, blank=True, related_name='created_by_customer_profiles')
    updated_by = models.ForeignKey('meghauth.MeghSvanaUser', on_delete=models.SET_NULL,
                                   null=True, blank=True, related_name='updated_by_customer_profiles')

    def __str__(self):
        return '#ID-%s - %s' % (self.id, self.customer)

    class Meta:
        verbose_name = 'Customer Profile'
        verbose_name_plural = 'Customer Profiles'


class CustomerCoinsModel(MeghSvanaUserCoinsModel):
    """
    Customer Wallet Proxy Model
    """
    objects = CustomerCoinsModelManager()

    class Meta:
        proxy = True
        verbose_name = 'Customer Coin Model'
        verbose_name_plural = 'Customer Coins Model'


class Order(models.Model):
    STATUS = [
        ('COM', 'Completed'),
        ('REJ', 'Rejected'),
        ('REF','Refunded'),
        ('INI', 'Initiated')
    ]
    SERVICES = [
        ('CH','Chat'),
        ('VI','Video'),
        ('RE','Report'),
        ('PR','Product'),
        ('CA','Call')
    ]
    oid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    customer = models.ForeignKey(CustomerUser, on_delete=models.CASCADE, related_name='order')
    services = models.CharField(max_length=70, choices=SERVICES)
    invoice = models.FileField(null=True, blank=True,)
    amount = models.DecimalField(decimal_places=3, null=True, blank=True, max_digits=6)
    status = models.CharField(max_length=50, choices=STATUS)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True, blank=True, related_name='created_by_orders')
    updated_by = models.ForeignKey('meghauth.MeghSvanaUser', on_delete=models.SET_NULL,
                                   null=True, blank=True, related_name='updated_by_orders')
    class Meta:
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'                               

           
