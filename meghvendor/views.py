from django.shortcuts import render
# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.
class ChatPage(LoginRequiredMixin, View):

    template_name = 'vendor/chat.html'

    def get(self, request):
        return render(request, self.template_name)

class ChatTablePage(LoginRequiredMixin, View):

    template_name = 'vendor/chat-table.html'

    def get(self, request):
        return render(request, self.template_name)

class UserWalletPage(LoginRequiredMixin, View):

    template_name = 'vendor/user-wallet.html'

    def get(self, request):
        return render(request, self.template_name)
class ManageUserPage(LoginRequiredMixin, View):

    template_name = 'vendor/manage-user.html'

    def get(self, request):
        return render(request, self.template_name)        
class PayrollPage(LoginRequiredMixin, View):

    template_name = 'vendor/payroll.html'

    def get(self, request):
        return render(request, self.template_name)        
class ScheduledAppointmentsPage(LoginRequiredMixin, View):

    template_name = 'vendor/scheduled-appointments.html'

    def get(self, request):
        return render(request, self.template_name)  
class ManageDiscountPage(LoginRequiredMixin, View):

    template_name = 'vendor/manage-discount.html'

    def get(self, request):
        return render(request, self.template_name)                                          
class ManageReportPage(LoginRequiredMixin, View):

    template_name = 'vendor/manage-report.html'

    def get(self, request):
        return render(request, self.template_name)                                          
class ServicesPage(LoginRequiredMixin, View):

    template_name = 'vendor/services.html'

    def get(self, request):
        return render(request, self.template_name)                                          
class WaitlistPage(LoginRequiredMixin, View):

    template_name = 'vendor/waitlist.html'

    def get(self, request):
        return render(request, self.template_name)                                          






