# -*- coding: utf-8 -*-

from django.db import models
from meghauth.models import (MeghSvanaUser, VendorUser, MeghSvanaUserCoinsModel)
from meghad.models import (VendorReportCategory,)
from meghvendor.managers import (VendorCoinsModelManager, )
from django.core.validators import RegexValidator
from django.contrib.postgres.fields import ArrayField
from meghvendor.validators import (validate_char_to_positive_integer,)
from mptt.models import (MPTTModel,
                         TreeForeignKey)


class VendorCategory(MPTTModel):
    """
    Category for vendors
    """
    title = models.CharField(max_length=255, help_text='Title for Vendor Category')
    slug = models.SlugField(max_length=200)
    parent = TreeForeignKey('self', on_delete=models.CASCADE , related_name='vendor_category', null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '#ID-%s - %s' % (self.id, self.slug)

    class Meta:
        verbose_name = 'Vendor Category'
        verbose_name_plural = 'Vendor Categories'


class VendorProfile(models.Model):
    """
    Profile Model: Vendor User Proxy Model
    """
    RATE_CHOICES = [
        ('PM', 'Per Min'),
        ('PH', 'Per Hour')
    ]
    STATUS_CHOICES = [
        ('OF', 'Offline'),
        ('ON', 'Online')
    ]
    vendor = models.OneToOneField(VendorUser, on_delete=models.CASCADE, related_name='vendor_profile')
    status = models.CharField(choices=STATUS_CHOICES, max_length=20, default='ON')
    category = models.ForeignKey(VendorCategory, on_delete=models.CASCADE, related_name='vendorprofiles', null=True, blank=True)
    usd_rate = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    inr_rate = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    usd_rate_type = models.CharField(choices=RATE_CHOICES, max_length=20, null=True, blank=True)
    inr_rate_type = models.CharField(choices=RATE_CHOICES, max_length=20, null=True, blank=True)
    aa_access = models.BooleanField(default=False, help_text='Toogle to indicate whether vendor can send Reports directly to Customers')
    experience = models.PositiveIntegerField(null=True, blank=True)
    rating = models.DecimalField(max_digits=2, decimal_places=1, null=True, blank=True)
    skills = ArrayField(models.CharField(max_length=10), null=True, blank=True)
    languages = ArrayField(models.CharField(max_length=20), null=True, blank=True)
    certificates = ArrayField(models.FileField(upload_to='vendors/certificates/'), null=True, blank=True)
    aadhar_no = models.CharField(max_length=12, null=True, blank=True, validators=[validate_char_to_positive_integer])
    aadhar_card = ArrayField(models.FileField(upload_to='vendors/aadhar_card/'), size=2, null=True, blank=True)
    pan_no = models.CharField(max_length=10, null=True, blank=True)
    pan_card = ArrayField(models.FileField(upload_to='vendors/pan_card/'), size=2, null=True, blank=True) 
    gst_no = models.CharField(max_length=15, null=True, blank=True)
    tax_remitence_file = models.FileField(upload_to='vendors/tax_remitence_file/', null=True, blank=True)
    cv = models.FileField(upload_to='vendors/cv/', null=True, blank=True)
    is_vc = models.BooleanField(default=False, help_text='Toogle to Indicate Vendor Provides Video call service')
    is_ac = models.BooleanField(default=False, help_text='Toogle to Indicate Vendor Provides Audio call service')
    is_ch = models.BooleanField(default=False, help_text='Toogle to Indicate Vendor Provides Chatting service')
    is_ls = models.BooleanField(default=False, help_text='Toogle to Indicate Vendor Provides Live Serivice service')
    is_tr = models.BooleanField(default=False, help_text='Toogle to Indicate Vendor Provides Text Report service')
    is_vr = models.BooleanField(default=False, help_text='Toogle to Indicate Vendor Provides Video Report service')
    unapproved_fields = ArrayField(models.CharField(max_length=20), null=True, blank=True)
    intro_video = models.FileField(upload_to='vendors/intro-video/', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   related_name='created_by_vendorprofiles',
                                   null=True,
                                   blank=True)
    updated_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   related_name='updated_by_vendorprofiles',
                                   null=True,
                                   blank=True)
    def __str__(self):
        return '#ID-%s - %s' % (self.id, self.vendor.email)


    def save(self, *args, **kwargs):
        """
        Custom save method for:
          :: If created_by field User = vendor field User all fields are unapproved.
        """
        if not self.pk:
            if self.created_by: 
                from general.utilities import FLAG_FIELDS
                self.unapproved_fields = FLAG_FIELDS
            else:
                pass
        super(VendorProfile, self).save(*args, **kwargs)
        
        
    class Meta:
        verbose_name = 'Vendor Profile'
        verbose_name_plural = 'Vendor Profiles'


class VendorReportSetting(models.Model):
    """
    Model to Store Price data for every report category
    the Vendor User can Provide to end User customer.
       :: Front end logic category cards for report will be seen
          then filter or searched by user , the vendor is listed 
          under the category with the rate list.
    """
    REPORT_MEDIUM_CHOICES = [
        ('V', 'Video'),
        ('T', 'Text')
    ]
    vendor = models.ForeignKey('meghauth.VendorUser', on_delete=models.CASCADE, related_name='vendor_report_settings')
    category = models.ForeignKey(VendorReportCategory, on_delete=models.CASCADE, related_name='vendor_report_settings')
    report_medium = models.CharField(max_length=50, choices=REPORT_MEDIUM_CHOICES)
    video_usd_rate = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    video_inr_rate = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    text_usd_rate = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    text_inr_rate = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    def __str__(self):
        return '#ID-%s - %s' % (self.id, self.vendor)
    
    class Meta:
        verbose_name = 'Vendor Report Setting'
        verbose_name_plural = 'Vendor Report Settings'


class VendorCoinsModel(MeghSvanaUserCoinsModel):
    """
    Vendor Wallet Proxy Model
    """
    objects = VendorCoinsModelManager()

    class Meta:
        proxy = True
        verbose_name = 'Vendor Coins'
        verbose_name_plural = 'Vendor Coins'
