# -*- coding: utf-8 -*-

from django.db import models
from meghauth.models import (MeghSvanaUserCoinsModel,)

class VendorCoinsModelManager(models.Manager):
    """
    Manager for Vendor Wallet
    Proxied from -> MeghSvanaUserWalletModel
    """
    def get_queryset(self):
        return super(VendorCoinsModelManager, self).get_queryset().filter(user__role='V') 
