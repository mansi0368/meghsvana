from django.apps import AppConfig


class MeghvendorConfig(AppConfig):
    name = 'meghvendor'
    verbose_name = 'Vendor Sub Application'
