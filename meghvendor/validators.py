# -*- coding: utf-8 -*-

from django.core.exceptions import ValidationError

def validate_char_to_positive_integer(value):
    try:
        int(value)
    except ValueError:
        raise ValidationError("Positive integer value is required")
                                
