# -*- coding: utf-8 -*-

from django.urls import re_path, path
from meghvendor import views

urlpatterns = [
    path('chat/', views.ChatPage.as_view(), name='chat'),
    path('chat-table/', views.ChatTablePage.as_view(), name='chat-table'),
    path('user-wallet/', views.UserWalletPage.as_view(), name='user-wallet'),  
    path('manage-user/', views.ManageUserPage.as_view(), name='manage-user'),
    path('payroll/', views.PayrollPage.as_view(), name='payroll'),
    path('scheduled-appointments/', views.ScheduledAppointmentsPage.as_view(), name='scheduled-appointments'),
    path('manage-discount/', views.ManageDiscountPage.as_view(), name='manage-discount'),
    path('services/', views.ServicesPage.as_view(), name='services'),
    path('waitlist/', views.WaitlistPage.as_view(), name='payroll'),
    path('manage-report/', views.ManageReportPage.as_view(), name='manage-report'),
  
]

