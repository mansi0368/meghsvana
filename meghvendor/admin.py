# -*- coding: utf-8 -*-

from django.contrib import admin
from meghauth.models import (VendorUser,)
from meghvendor.models import (VendorProfile,
                               VendorCategory,
                               VendorReportSetting,
                               VendorCoinsModel)
from mptt.admin import MPTTModelAdmin

class VendorProfileAdmin(admin.ModelAdmin):

    model = VendorProfile
    fieldsets = (
        ('Vendor User', {'fields': ('vendor',)}),
        ('Category', {'fields': ('category',)}),
        ('Personal Information', {'fields': ('languages',)}),
        ('Official Information', {'fields': ('skills', 'cv', 'intro_video','aadhar_no', 'aadhar_card', 'pan_no', 'pan_card', 'gst_no', 'tax_remitence_file')}),
        ('Service Rates', {'fields': ('usd_rate', 'inr_rate', 'usd_rate_type', 'inr_rate_type')}),
        ('Services List', {'fields': ('is_vc', 'is_ac', 'is_ch', 'is_ls', 'is_tr', 'is_vr')}),
        ('Created By', {'fields': ('created_by',)}),
        ('Updated By', {'fields': ('updated_by',)}),
        ('Dates', {'fields': ('created_at', 'updated_at')}),
    )
    readonly_fields = ('created_at', 'updated_at', 'created_by', 'updated_by')
    list_display = ('vendor','created_at', 'created_by')
    
    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        elif change:
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)

    def has_add_permission(self, request):
        return False


class VendorReportSettingAdmin(admin.ModelAdmin):

    model = VendorReportSetting
    fields = ('vendor', 'category', 'report_medium', 'video_usd_rate', 'video_inr_rate',
              'text_usd_rate', 'text_inr_rate', 'created_at', 'updated_at')
    readonly_fields = ('created_at', 'updated_at')
    list_display = ('vendor', 'category', 'report_medium', 'video_usd_rate', 'video_inr_rate',
              'text_usd_rate', 'text_inr_rate', 'created_at')


class MPTTModelAdmin(admin.ModelAdmin):

    model = VendorCategory
    fields = ('title','parent', 'slug', 'description', 'created_at', 'updated_at')
    readonly_fields = ('created_at', 'updated_at')
    list_display = ('title', 'slug', 'created_at', 'updated_at')


class VendorCoinsModelAdmin(admin.ModelAdmin):

    model = VendorCoinsModel
    fields = ('uiid', 'coins', 'points', 'total_spent', 'user', 'created_at', 'updated_at')
    list_display = ('uiid', 'coins', 'points', 'total_spent', 'user', 'created_at', 'updated_at')
    readonly_fields = ('uiid', 'created_at', 'updated_at')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'user':
            kwargs['queryset'] = VendorUser.objects.filter(is_active=True)
        return super(VendorCoinsModelAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

# Register your models here.
admin.site.register(VendorProfile, VendorProfileAdmin)
admin.site.register(VendorCategory, MPTTModelAdmin)
admin.site.register(VendorReportSetting, VendorReportSettingAdmin)
admin.site.register(VendorCoinsModel, VendorCoinsModelAdmin)
