# -*- coding: utf-8 -*-

from django.urls import re_path, path
from website import views

urlpatterns = [
    path('', views.HomePageView.as_view(), name='home')
]
