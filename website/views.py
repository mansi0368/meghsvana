# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.

class HomePageView(LoginRequiredMixin, View):

    template_name = 'website/index.html'

    def get(self, request):
        return render(request, self.template_name)
