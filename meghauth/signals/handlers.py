# -*- coding: utf-8 -*-

from meghad.models import (VendorServicePermission)
from meghvendor.models import (VendorProfile)
from meghauth.models import (MeghSvanaUser)
from django.db.models.signals import pre_save


def post_save_vendor(sender, instance, created, *args, **kwargs):
    if created:
        VendorServicePermission.objects.create(vendor=instance)
        VendorProfile.objects.create(vendor=instance, created_by=instance.created_by)
    else:
        pass
 
def create_log_before_saving(sender, instance, *args, **kwargs):
    if instance.id is None:
        pass
    else:
        current = instance
        previous = MeghSvanaUser.objects.get(id=instance.id)

def create_log_before_saving(sender, *args, **kwargs):
    meghsvanauser_instance = kwargs['instance']
    if meghsvanauser_instance.id:
       meghsvanauser_db = MeghSvanaUser.objects.get(id=meghsvanauser_instance.id)

           
        
