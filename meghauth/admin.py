# -*- coding: utf-8 -*-

from django.contrib import admin
from django.contrib.auth.models  import Group
from django.contrib.auth.admin import UserAdmin
from meghauth.models import (MeghSvanaUser,
                             VendorUser,
                             CustomerUser,
                             AdminUser,
                             MeghSvanaUserCoinsModel)
from meghauth.forms import (MeghSvanaUserCreationForm, MeghSvanaUserChangeForm,
                            VendorUserCreationForm, CustomerUserCreationForm, AdminUserCreationForm)


class MeghSvanaUserAdmin(UserAdmin):
    form = MeghSvanaUserChangeForm
    add_form = MeghSvanaUserCreationForm

    list_display = ('email', 'full_name', 'is_active','is_superuser', 'is_staff', 'role', 'date_joined', 'last_login', 'created_by', 'updated_by')
    list_filter = ('role', 'is_active', 'is_staff', 'is_superuser')

    fieldsets = (
        ('MeghSvana User', {'fields': ('email', 'password')}),
        ('Personal Information', {'fields': ('username','full_name', 'profile_picture', 'dob')}),
        ('User Type', {'fields': ('role', 'is_active', 'is_superuser', 'is_staff')}),
        ('Contact Information', {'fields': ('p_phone', 's_phone', 'country', 'country_code')}),
        ('Group', {'fields': ('groups',)}),
        ('Permissions', {'fields': ('user_permissions',)}),
        ('Created By', {'fields': ('created_by',)}),
        ('Updated By', {'fields': ('updated_by',)}),
        ('Dates', {'fields': ('last_login', 'date_joined')}),
    )
    readonly_fields = ('created_by', 'updated_by')
    add_fieldsets = (
        ('Add MeghSvana User', {
            'classes': ('wide',),
            'fields': ('full_name', 'email', 'password', 'confirm_password'),
        }),
    )
    search_field = ('email', 'full_name')
    
    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        elif change:
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class VendorUserAdmin(UserAdmin):
    form = MeghSvanaUserChangeForm
    add_form = VendorUserCreationForm

    list_display = ('email', 'full_name', 'is_active','is_superuser', 'is_staff', 'date_joined', 'last_login', 'created_by', 'updated_by')
    list_filter = ('is_active', 'is_staff', 'is_superuser')

    fieldsets = (
        ('Vendor User', {'fields': ('email', 'password')}),
        ('Personal Information', {'fields': ('username', 'full_name', 'profile_picture', 'dob')}),
        ('User Type', {'fields': ('is_active', 'is_superuser', 'is_staff')}),
        ('Contact Information', {'fields': ('p_phone', 's_phone', 'country', 'country_code')}),
        ('Group', {'fields': ('groups',)}),
        ('Permissions', {'fields': ('user_permissions',)}),
        ('Created By', {'fields': ('created_by',)}),
        ('Updated By', {'fields': ('updated_by',)}),
        ('Dates', {'fields': ('last_login', 'date_joined')}),
    )
    readonly_fields = ('created_by', 'updated_by')
    add_fieldsets = (
        ('Add Vendor User', {
            'classes': ('wide',),
            'fields': ('full_name', 'email', 'password', 'confirm_password'),
        }),
    )
    search_field = ('email', 'full_name')
    
    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        elif change:
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class CustomerUserAdmin(UserAdmin):
    form = MeghSvanaUserChangeForm
    add_form = CustomerUserCreationForm

    list_display = ('email', 'full_name', 'is_active','is_superuser', 'is_staff', 'date_joined', 'last_login', 'created_by', 'updated_by')
    list_filter = ('is_active', 'is_staff', 'is_superuser')

    fieldsets = (
        ('Customer User', {'fields': ('email', 'password')}),
        ('Personal Information', {'fields': ('username', 'full_name', 'profile_picture', 'dob')}),
        ('User Type', {'fields': ('is_active', 'is_superuser', 'is_staff')}),
        ('Contact Information', {'fields': ('p_phone', 's_phone', 'country', 'country_code')}),
        ('Group', {'fields': ('groups',)}),
        ('Permissions', {'fields': ('user_permissions',)}),
        ('Created By', {'fields': ('created_by',)}),
        ('Updated By', {'fields': ('updated_by',)}),
        ('Dates', {'fields': ('last_login', 'date_joined')}),
    )
    readonly_fields = ('created_by', 'updated_by')
    add_fieldsets = (
        ('Add Customer User', {
            'classes': ('wide',),
            'fields': ('full_name', 'email', 'password', 'confirm_password'),
        }),
    )
    search_field = ('email', 'full_name')
    
    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        elif change:
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)


class AdminUserAdmin(UserAdmin):
    form = MeghSvanaUserChangeForm
    add_form = AdminUserCreationForm

    list_display = ('email', 'full_name', 'is_active','is_superuser', 'is_staff', 'date_joined', 'last_login', 'created_by', 'updated_by')
    list_filter = ('is_active', 'is_staff', 'is_superuser')

    fieldsets = (
        ('Admin User', {'fields': ('email', 'password')}),
        ('Personal Information', {'fields': ('username', 'full_name', 'profile_picture', 'dob')}),
        ('User Type', {'fields': ('is_active', 'is_superuser', 'is_staff')}),
        ('Contact Information', {'fields': ('p_phone', 's_phone', 'country', 'country_code')}),
        ('Group', {'fields': ('groups',)}),
        ('Permissions', {'fields': ('user_permissions',)}),
        ('Created By', {'fields': ('created_by',)}),
        ('Updated By', {'fields': ('updated_by',)}),
        ('Dates', {'fields': ('last_login', 'date_joined')}),
    )
    readonly_fields = ('created_by', 'updated_by')
    add_fieldsets = (
        ('Add Customer User', {
            'classes': ('wide',),
            'fields': ('full_name', 'email', 'password', 'confirm_password'),
        }),
    )
    search_field = ('email', 'full_name')
    
    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        elif change:
            obj.updated_by = request.user
        super().save_model(request, obj, form, change)



class MeghSvanaUserCoinsModelAdmin(admin.ModelAdmin):

    model = MeghSvanaUserCoinsModel
    fields = ('uiid', 'coins', 'points', 'total_spent', 'user', 'created_at', 'updated_at')
    list_display = ('uiid', 'coins', 'points', 'total_spent', 'user', 'created_at', 'updated_at')
    readonly_fields = ('uiid', 'created_at', 'updated_at')
    
# Register your models here.

admin.site.register(MeghSvanaUser, MeghSvanaUserAdmin)
admin.site.register(VendorUser, VendorUserAdmin)
admin.site.register(CustomerUser, CustomerUserAdmin)
admin.site.register(AdminUser, AdminUserAdmin)
admin.site.register(MeghSvanaUserCoinsModel, MeghSvanaUserCoinsModelAdmin)
