# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.views import View
from django.contrib.auth import (authenticate, login, logout)


class LoginView(View):
    """
    Login View Page For meghSvana
    """
    template_name = 'auth/login.html'
    
    def get(self, request):
        if request.user.is_authenticated:
            return redirect('home')
        else:
            return render(request, self.template_name)

    def post(self, request):
        user = authenticate(email=request.POST.get('email'), password=request.POST.get('password'))
        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            return render(request,
                          self.template_name,
                          context={'error': 'Invalid Credentails'},
                          status=400)

class LogoutView(View):
    
    def get(self, request):
        logout(request)
        return redirect('login')
