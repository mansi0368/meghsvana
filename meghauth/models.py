# -*- coding: utf-8 -*-

import uuid
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.core.exceptions import ValidationError
from django.contrib.auth.models import PermissionsMixin
from django.utils import timezone
from django.core.validators import RegexValidator
from meghauth.managers import (MeghSvanaUserModelManager,
                               VendorUserModelManager,
                               CustomerUserModelManager, AdminUserModelManager)

# Create your models here.

class MeghSvanaUser(AbstractBaseUser, PermissionsMixin):
    """
    Custom User Model for all types of meghsvana Users
    """
    USER_ROLE_CHOICES = [
        ('A', 'Admin'),
        ('V', 'Vendor'),
        ('CS', 'Customer'),
    ]
    phone_regex = RegexValidator(regex=r'^\d{0,10}$',
                                 message="Phone number must be entered in the format:9874XXX678. Up to 10 digits are allowed.")
    profile_picture = models.FileField(upload_to="vendors/profile-pictures/", null=True, blank=True)
    email = models.EmailField(max_length=255, null=False, blank=False,
                              unique=True, db_index=True, help_text='Email of megh svana user, must be unique.')
    full_name = models.CharField(max_length=255)
    dob = models.DateField(null=True, blank=True)
    p_phone = models.CharField(validators=[phone_regex], max_length=10, null=True, blank=True)
    s_phone = models.CharField(validators=[phone_regex], max_length=10, null=True, blank=True)
    country = models.CharField(max_length=100, null=True, blank=True)
    country_code = models.CharField(max_length=5, null=True, blank=True)
    username = models.CharField(max_length=255, null=True, blank=True, unique=True, help_text='Username of megh svana user, must be unique.')
    role = models.CharField(choices=USER_ROLE_CHOICES, max_length=20, null=True, blank=True, help_text='Used to set role for MeghSvana User')
    is_active = models.BooleanField(default=True, help_text='Set active/inactive, InActive users cannot login.')
    is_staff = models.BooleanField(default=False, help_text='Set true to enable Django Default Admin interface for the user')
    is_superuser = models.BooleanField(default=False, help_text='Grant (Superuser access): All CRUD permissions on every registered model.')
    created_by = models.ForeignKey('MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   related_name='created_by_meghusers',
                                   null=True,
                                   blank=True)
    updated_by = models.ForeignKey('MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   related_name='updated_by_meghusers',
                                   null=True,
                                   blank=True)
    last_login = models.DateTimeField(null=True, blank=True)
    date_joined = models.DateTimeField(default=timezone.now)
    about = models.TextField(null=True, blank=True)
    objects = MeghSvanaUserModelManager()
    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['full_name']

    class Meta:
        verbose_name = 'MeghSvana User'
        verbose_name_plural = 'MeghSvana Users'


class MeghSvanaUserCoinsModel(models.Model):
    """
    Generic model for Coins for every meghsvana user to store coins as
    balance
    """
    uiid = models.UUIDField(default=uuid.uuid4, editable=False)
    coins = models.DecimalField(max_digits=15, decimal_places=2, help_text='One ruppee is equal to One Coin')
    points = models.DecimalField(max_digits=3, decimal_places=0)
    total_spent = models.DecimalField(max_digits=9, decimal_places=2)
    user = models.ForeignKey(MeghSvanaUser, on_delete=models.CASCADE, related_name='wallets')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    def __str__(self):
        return '#ID-%s - %s' % (self.id, self.user)

    class Meta:
        verbose_name = 'User Wallet'
        verbose_name_plural = 'User Wallets'


        
class VendorUser(MeghSvanaUser):
    """
    Proxy Model: MeghSvanaUser with (role = V:Vendor) (Vendor User)
    """
    objects = VendorUserModelManager()

    class Meta:
        proxy = True
        verbose_name = 'Vendor User'
        verbose_name_plural = 'Vendor Users'


class CustomerUser(MeghSvanaUser):
    """
    Proxy Model: MeghSvanaUser with (role = CS:Customer) (Customer User)
    """

    objects = CustomerUserModelManager()

    class Meta:
        proxy = True
        verbose_name = 'Customer User'
        verbose_name_plural = 'Customer Users'


class AdminUser(MeghSvanaUser):
    """
    Proxy Model: MeghSvanaUser with (role = A:Admin) (Admin User)
    """

    objects = AdminUserModelManager()

    class Meta:
        proxy = True
        verbose_name = 'Admin User'
        verbose_name_plural = 'Admin Users'
