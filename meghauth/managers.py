# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.base_user import BaseUserManager


class MeghSvanaUserModelManager(BaseUserManager):
    """
    User Manager used by MeghSvana Custom User Model.
    """
    def create_user(self, email, full_name, password=None):
        if not email:
            raise ValidationError("Email is required.")
        if not password or password is None:
            raise ValidationError("Password is required.")
        
        user = self.model(email=self.normalize_email(email), full_name=full_name)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, full_name, password=None):
        """                                                                                                                                                                                               
        Method used to create the superuser.                                                                                                                                                                     
        It is initiated whenever somone calls                                                                                                                                                                     
        createsuperuser manage command.                                                                                                                                                                           
        """
        user = self.create_user(email=email, full_name=full_name, password=password)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class VendorUserModelManager(BaseUserManager):
    """
    User Manager used by Vendor User Proxy Model.
    """
    def get_queryset(self):
        return super(VendorUserModelManager, self).get_queryset().filter(role__exact='V')

    def create(self, **kwargs):
        kwargs['role'] = 'V'
        return super(VendorUserModelManager, self).create(**kwargs)


class CustomerUserModelManager(BaseUserManager):
    """
    User Manager used by Customer User Proxy Model.
    """
    def get_queryset(self):
        return super(CustomerUserModelManager, self).get_queryset().filter(role__exact='CS')

    def create(self, **kwargs):
        kwargs['role'] = 'CS'
        return super(CustomerUserModelManager, self).create(**kwargs)


class AdminUserModelManager(BaseUserManager):
    """
    User manager used by Admin User Proxy Model.
    """

    def get_queryset(self):
        return super(AdminUserModelManager, self).get_queryset().filter(role__exact='A')

    def create(self, **kwargs):
        kwargs['role'] = 'A'
        return super(AdminUserModelManager, self).create(**kwargs)
