# -*- coding: utf-8 -*-

from django.apps import AppConfig
from django.db.models.signals import pre_save




class MeghauthConfig(AppConfig):
    name = 'meghauth'
    verbose_name = 'Megh Auth Sub Application'

    def ready(self):
        from django.db.models.signals import post_save
        from meghauth.models import (VendorUser)
        from meghauth.signals.handlers import (post_save_vendor)
        from meghauth.models import MeghSvanaUser
        from meghauth.signals.handlers import (create_log_before_saving)
       
        
        post_save.connect(post_save_vendor, sender=VendorUser)
        pre_save.connect(create_log_before_saving, sender=MeghSvanaUser)
        