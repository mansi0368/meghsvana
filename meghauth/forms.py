# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from meghauth.models import (MeghSvanaUser, CustomerUser, VendorUser, AdminUser)


class MeghSvanaUserCreationForm(forms.ModelForm):

    password = forms.CharField(label='Password', widget=forms.PasswordInput)
    confirm_password = forms.CharField(label='Confirm Password', widget=forms.PasswordInput)

    class Meta:
        model = MeghSvanaUser
        fields = ('email', 'full_name')

    def clean(self):
        super(MeghSvanaUserCreationForm, self).clean()
        user_pass = self.cleaned_data.get('password')
        confirm_user_pass = self.cleaned_data.get('confirm_password')

        if user_pass and confirm_user_pass and user_pass != confirm_user_pass:
            raise forms.ValidationError("Password and Confirm Password don't match.")
        else:
            pass

    def save(self, commit=True):
        user = super(MeghSvanaUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data.get('password'))

        if commit:
            user.save()
        return user


class VendorUserCreationForm(MeghSvanaUserCreationForm):

    class Meta:
        model = VendorUser
        fields = ('email', 'full_name')

    def save(self, commit=True):
        user = super(VendorUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data.get('password'))
        user.role = 'V'
        
        if commit:
            user.save()
        return user
        

class CustomerUserCreationForm(MeghSvanaUserCreationForm):

    class Meta:
        model = CustomerUser
        fields = ('email', 'full_name')


    def save(self, commit=True):
        user = super(CustomerUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data.get('password'))
        user.role = 'CS'
        
        if commit:
            user.save()
        return user


class AdminUserCreationForm(MeghSvanaUserCreationForm):

    class Meta:
        model = AdminUser
        fields = ('email', 'full_name')

    def save(self, commit=True):
        user = super(AdminUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data.get('password'))
        user.role = 'A'

        if commit:
            user.save()
        return user


class MeghSvanaUserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(label=("Password"),
                                         help_text=("""Django does not stores password in readable form,                                                                                                          
                                         So you cannot see this user's password, but you can change the password                                                                                                  
                                         using <a href=\"../password/\">this form</a>."""))
    class Meta:
        model = MeghSvanaUser
        fields = ('email', 'password')

    def clean_password(self):
        return self.initial['password']
