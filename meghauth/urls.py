# -*- coding: utf-8 -*-

from django.urls import re_path, path
from meghauth import views

urlpatterns = [
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
]
