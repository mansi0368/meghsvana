# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.
class ManageVendorsPage(LoginRequiredMixin, View):

    template_name = 'meghad/manage-vendors.html'

    def get(self, request):
        return render(request, self.template_name)


class AddVendorPage(LoginRequiredMixin, View):

    template_name = 'meghad/add-vendor.html'

    def get(self, request):
        return render(request, self.template_name)


class VendorCategory(LoginRequiredMixin, View):

    template_name = 'meghad/vendor-categories.html'

    def get(self, request):
        return render(request, self.template_name)


