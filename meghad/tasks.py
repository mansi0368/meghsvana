# -*- coding: utf-8 -*-

from celery import shared_task
from meghadmin.models import (TaskModel,)
from django.utils import timezone

@shared_task
def _mark_task_delay():
    """
    Task to change status of task to D --> DELAYED
     -- Filter for Pending status tasks with due date > timezone.now()
     -- Bulk update with status = D
    """
    return TaskModel.objects.filter(status='P', due_date_time__gte=timezone.now()).update(status='D')
    
