# -*- coding: utf-8 -*-

from django.urls import re_path, path
from meghad import views

urlpatterns = [
    path('manage-vendors/', views.ManageVendorsPage.as_view(), name='manage-vendors'),
    path('vendor-category/', views.VendorCategory.as_view(), name='vendor-category'),
    path('add-vendor/', views.AddVendorPage.as_view(), name='add-vendor')
]
