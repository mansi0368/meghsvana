# -*- coding: utf-8 -*-

from django.contrib import admin
from meghad.models import (VendorReportCategory,
                              VendorServicePermission,
                              TaskCategory,
                              TaskModel, 
                              Activity,
                              Notice,
                              Payroll)
from meghauth.models import (VendorUser,)
class VendorReportCategoryAdmin(admin.ModelAdmin):

    model = VendorReportCategory
    fields = ('name', 'created_at', 'updated_at')
    readonly_fields = ('created_at', 'updated_at')
    list_display = ('id', 'name', 'created_at', 'updated_at')

class VendorServicePermissionAdmin(admin.ModelAdmin):

    model = VendorServicePermission
    fields = ('is_vc', 'is_ac', 'is_ch', 'is_ls', 'is_tr', 'is_vr', 'vendor', 'created_at','updated_at') 
    readonly_fields = ('created_at', 'updated_at') 
    list_display = ('id', 'is_vc', 'is_ac', 'is_ch', 'is_ls', 'is_tr', 'is_vr', 'vendor', 'created_at','updated_at')
    list_display = ('is_vc', 'is_ac', 'is_ch', 'is_ls', 'is_tr', 'is_vr', 'vendor', 'created_at','updated_at')

class ActivityAdmin(admin.ModelAdmin):

    model = Activity
    fields = ('user', 'sender', 'operation_type', 'old_data', 'new_data', 'created_at','updated_at') 
    readonly_fields = ('created_at', 'updated_at')
    list_display = ('user', 'sender', 'operation_type', 'created_at', 'updated_at') 

class TaskCategoryAdmin(admin.ModelAdmin):

    model = TaskCategory
    fields = ('name', 'created_at', 'updated_at')
    readonly_fields = ('created_at', 'updated_at')
    list_display = ('id', 'name', 'created_at', 'updated_at')

class TaskModelAdmin(admin.ModelAdmin):

    model = TaskModel
    fields = ('title', 'ass_with', 'status', 'due_date_time', 'reminder_method', 'category',
              'task_details', 'created_at', 'updated_at', 'created_by', 'updated_by')
    readonly_fields = ('created_at', 'updated_at', 'created_by', 'updated_by')
    list_display = ('id', 'title', 'status', 'created_at', 'updated_at', 'created_by')
    list_filters = ('status',)
class NoticeAdmin(admin.ModelAdmin):

    model = Notice
    fields = ('topic', 'description', 'created_at', 'updated_at', 'created_by', 'updated_by')
    readonly_fields = ('created_at', 'updated_at', 'created_by', 'updated_by')
    list_display = ('topic', 'description', 'created_at', 'updated_at', 'created_by', 'updated_by')
class PayrollAdmin(admin.ModelAdmin):

    model = Payroll
    fields = ('name', 'total_work_duration', 'gross_pay', 'net_pay', 'mode', 'tax', 'status' 'created_at', 'updated_at', 'created_by', 'updated_by')
    readonly_fields = ('created_at', 'updated_at', 'created_by', 'updated_by')
    list_display = ('name', 'created_at', 'updated_at', 'created_by', 'updated_by')        

# Register your models here.
admin.site.register(VendorReportCategory, VendorReportCategoryAdmin)
admin.site.register(VendorServicePermission, VendorServicePermissionAdmin)
admin.site.register(TaskCategory, TaskCategoryAdmin)
admin.site.register(TaskModel, TaskModelAdmin)
admin.site.register(Activity, ActivityAdmin)
admin.site.register(Notice, NoticeAdmin)
admin.site.register(Payroll, PayrollAdmin)


