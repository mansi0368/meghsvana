# -*- coding: utf-8 -*-

from django.apps import AppConfig


class MeghadminConfig(AppConfig):

    name = 'meghadmin'
    verbose_name = 'MeghSvana Admin Sub Application'
        
