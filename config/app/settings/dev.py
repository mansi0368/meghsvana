# -*- coding: utf-8 -*-

import os
from config.app.settings.base import *  # NOQA
import logging
import logging.config as logging_config
from colorlog import ColoredFormatter


INSTALLED_APPS += ['django_extensions',]

# logging setup for the application
#LOGGING_CONFIG = None
#logging_config.dictConfig({
 #   'version': 1,
  #  'disable_existing_loggers': False,
   # 'formatters': {
    #    'colored': {
     #       '()': 'colorlog.ColoredFormatter',
      #      'format': "%(log_color)s%(levelname)-8s%(reset)s %(blue)s%(message)s",
       #     'datefmt': None,
      #      'reset': True,
      #      'log_colors':{
      #          'DEBUG':    'cyan',
      #          'INFO':     'green,bg_yellow',
      #          'WARNING':  'yellow,bg_white',
      #          'ERROR':    'red',
      #          'CRITICAL': 'red,bg_white',
      #      },
      #      'secondary_log_colors':{
      #          'message': {
      #              'ERROR':    'red',
      #              'CRITICAL': 'red'
      #          }
      #      },
      #      'style':'%'
      #  }
    #},
    #'handlers': {
    #    'console': {
     #       'class': 'logging.StreamHandler',
     #       'formatter': 'colored'
      #  },
       # 'file': {
       #     'class': 'logging.FileHandler',
       #     'formatter': 'colored',
       #     'filename': '/tmp/ltms_dev.log'
       # }
   # },
   # 'loggers': {
   #     '': {
   #         'level': 'INFO',
   #         'handlers': ['console', 'file']
   #     },
   #     'django.request': {
   #         'level': 'DEBUG',
   #         'handlers': ['console', 'file']
   #     }
   # }
#})


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'meghsvana_dev_db',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        }
    }
