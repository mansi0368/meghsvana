# -*- coding: utf-8 -*-

import os
from config.app.settings.base import *  # NOQA
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
# settings up inports for logging
import logging
import logging.config as logging_config
from colorlog import ColoredFormatter


DEBUG = False

INSTALLED_APPS += ['storages', ]
ALLOWED_HOSTS = ['api.meghsvana.com', ]

# logging setup for the application
LOGGING_CONFIG = None

logging_config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'colored': {
            '()': 'colorlog.ColoredFormatter',
            'format': "%(log_color)s%(levelname)-8s%(reset)s %(blue)s%(message)s",
            'datefmt': None,
            'reset': True,
            'log_colors':{
                'DEBUG':    'cyan',
                'INFO':     'green,bg_yellow',
                'WARNING':  'yellow,bg_white',
                'ERROR':    'red',
                'CRITICAL': 'red,bg_white',
            },
            'secondary_log_colors':{
                'message': {
                    'ERROR':    'red',
                    'CRITICAL': 'red'
                }
            },
            'style':'%'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'colored'
        }
    },
    'loggers': {
        '': {
            'level': 'INFO',
            'handlers': ['console',]
        },
        'django.request': {
            'level': 'DEBUG',
            'handlers': ['console',]
        }
    }
})


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get("SQL_DATABASE", None),
        'USER': os.environ.get("SQL_USER", None),
        'PASSWORD': os.environ.get("SQL_USER_PASSWORD", None),
        'HOST': os.environ.get("SQL_HOST", None),
        'PORT': os.environ.get("SQL_PORT", None),
    }
}


if os.environ.get("SENTRY_DSN", None) is not None:
    sentry_sdk.init(
        dsn=os.environ.get("SENTRY_DSN", None),
        integrations=[DjangoIntegration()],
        traces_sample_rate=1.0,
        # If you wish to associate users to errors (assuming you are using
        # django.contrib.auth) you may enable sending PII data.
        send_default_pii=True,
        environment="production"
    )
else:
    # if SENTDRY_DSN doesn't exist
    # in environment variable
    pass



# Compression Related
# settings.
AWS_IS_GZIPPED = True
COMPRESS_ENABLED = False
HTML_MINIFY = False


AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = os.environ.get('AWS_STORAGE_BUCKET_NAME')
AWS_DEFAULT_ACL = 'public-read'
AWS_S3_CUSTOM_DOMAIN = f'{AWS_STORAGE_BUCKET_NAME}.s3.amazonaws.com'
AWS_S3_OBJECT_PARAMETERS = {'CacheControl': 'max-age=86400'}

# static files settings
AWS_STATIC_LOCATION = 'static'
STATIC_URL = f'https://{AWS_S3_CUSTOM_DOMAIN}/{AWS_STATIC_LOCATION}/'
COMPRESS_URL = STATIC_URL
STATICFILES_STORAGE = 'meghsvana.storages.CachedS3Boto3Storage'

# Media files settings
AWS_MEDIA_LOCATION = 'media'
MEDIA_URL = f'https://{AWS_S3_CUSTOM_DOMAIN}/{AWS_MEDIA_LOCATION}/'
ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'
DEFAULT_FILE_STORAGE = 'meghsvana.storages.MediaStorage'
