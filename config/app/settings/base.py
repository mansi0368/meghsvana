# -*- coding: utf-8 -*-

from pathlib import Path
import os
from datetime import timedelta

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

BASE_DIR = Path(__file__).resolve().parent.parent.parent.parent
SECRET_KEY = '(6!^ylm%0e*t(=5jn=cgurt=^%$c%7+#d1*crvfgc35__em!fj'
ALLOWED_HOSTS = ['localhost', '127.0.0.1']


# Application definition

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

# Custom enabled applications
LOCAL_APPS = [
    'meghauth',
    'meghad',
    'meghvendor',
    'meghcustomer',
    'general',
    'meghchatserver',
]

# Default DRF enabled aplications
REST_FRAMEWORK_APPS = [
    'rest_framework',
    'drf_yasg',
]

# Third Party enabled modules 
THIRD_PARTY_APPS = [
    'corsheaders',
    'django_celery_beat',
    'channels',
]

INSTALLED_APPS = DJANGO_APPS +  REST_FRAMEWORK_APPS + THIRD_PARTY_APPS + LOCAL_APPS 


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]


ROOT_URLCONF = 'meghsvana.urls'


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# Wsgi setting for meghsvana
WSGI_APPLICATION = 'meghsvana.wsgi.application'

# Asgi setting for meghsvana
ASGI_APPLICATION = 'meghsvana.routing.application'

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Asia/Kolkata'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static'), ]

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'


# COMPRESS_ENABLED = False
# HTML_MINIFY = False
# COMPRESS_CSS_HASHING_METHOD = 'content'


STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'compressor.finders.CompressorFinder',
)

#RestFrameworkSettings
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework_simplejwt.authentication.JWTAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ]
}


LOG_PATH = os.path.join(BASE_DIR, 'log')

# Auth User model Settings
AUTH_USER_MODEL = 'meghauth.MeghSvanaUser'

# Celery config
CELERY_BROKER_URL = 'redis://%s:6379' % (os.environ.get('REDIS_HOST', '127.0.0.1'))
CELERY_RESULT_BACKEND = 'redis://%s:6379' % (os.environ.get('REDIS_HOST', '127.0.0.1'))
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = TIME_ZONE


# Channel Layer settings for Django channel

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [(os.environ.get('REDIS_HOST', "127.0.0.1"), 6379)],
        },
    },
}

LOGIN_URL = '/auth/login/'

