# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.serializers import (ModelSerializer, SerializerMethodField, ValidationError)
from meghauth.models import CustomerUser
from meghcustomer.models import (CustomerProfile, CustomerCoinsModel)


class CustomerUserModelSerializer(ModelSerializer):

    class Meta:
        model = CustomerUser
        fields = ('id', 'email', 'full_name', 'username', 'dob',
                  'country', 'country_code', 'p_phone', 's_phone', 'about')


class CustomerUserProfileModelSerializer(ModelSerializer):
    cutomer_user = SerializerMethodField()

    def get_customer_user(self, instance):
        return CustomerUserModelSerializer(instance.customer).data
    
    class Meta:
        model = CustomerProfile
        fields = '__all__'


class CustomerCoinsModelSerializer(ModelSerializer):

    class Meta:
        model = CustomerCoinsModel
        fields = '__all__'
