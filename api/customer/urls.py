# -*- coding: utf-8 -*-

from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import url
from api.vendor import views

router = DefaultRouter()

router.register('customer-user', views.CustomerUserModelViewSet, basename='customer-user')
router.register('customer-profile', views.CustomerProfileModelViewSet, basename='customer-profile')
router.register('customer-coins', views.CustomerCoinsModelViewSet, basename='customer-coins')
