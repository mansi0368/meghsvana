# -*- coding: utf-8 -*-

from rest_framework import status
from rest_framework.viewsets import (ModelViewSet,)
from rest_framework.permissions import (AllowAny, IsAuthenticated)
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.pagination PageNumberPagination
from meghauth.models import (MeghSvanaUser, VendorUser)
from meghcustomer.models import (CustomerProfile, CustomerCoinsModel)
from api.customer.serializers import (CustomerUserModelSerializer, CustomerUserProfileModelSerializer)

# Create your views here.

class CustomerUserModelViewSet(ModelViewSet):

    queryset = CustomerUser.objects.all()
    serializer_class = CustomerUserModelSerializer
    permission_classes = [IsAuthenticated,]

    def get_queryset(self):
        return self.queryset.filter(id=self.request.user.id)


class CustomerProfileModelViewSet(ModelViewSet):

    queryset = CustomerProfile.objects.all()
    serializer_class = CustomerUserProfileModelSerializer
    permission_classes = [IsAuthenticated,]

    def get_queryset(self):
        return self.queryset.filter(customer=self.request.user)


class CustomerCoinsModelViewSet(ModelViewSet):

    queryset = CustomerCoinsModel.objects.all()
    serializer_class = CustomerCoinsModelSerializer
    permission_classes = [IsAuthenticated,]
