# -*- coding: utf-8 -*-

from rest_framework import views, viewsets, status
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from api.general.serializers import (TicketModelSerializer, DiscountModelSerializer, BlogModelSerializer)
from general.models import (Ticket, Discount, VendorReview, Blog)
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated
from api.vendor.serializers import (VendorReviewTableModelSerializer, ChatThreadModelSerializer,
                                    CustomerChatRequestTableModelSerializer, ChatMessageModelSerializer,
                                    CustomerChatRequestModelSerializer)
from django.db.models.query_utils import Q
from meghchatserver.models import (ChatMessage, ChatThread,
                                   CustomerChatRequest)


class DiscountModelViewSet(ModelViewSet):
    
    queryset = Discount.objects.all()
    serializer_class = DiscountModelSerializer
    permission_classes = [IsAuthenticated,]   

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)


class TicketModelViewSet(ModelViewSet):

    queryset = Ticket.objects.all()
    serializer_class = TicketModelSerializer
    permission_classes = [IsAuthenticated,]   

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)  


class VendorReviewTableModelViewSet(ModelViewSet):

    queryset = VendorReview.objects.all()
    serializer_class = VendorReviewTableModelSerializer
    pagination_class = PageNumberPagination
    http_method_names = ['get', 'head', 'options']
    permission_classes = [IsAuthenticated,]

    def get_queryset(self):
        query = self.request.query_params.get('search', None)
        if query:
            return self.queryset.filter(vendor=self.request.user).filter(Q(review__icontains=query) | Q(vendor__email=query))
        else:
            return self.queryset.filter(vendor=self.request.user)


class BlogModelViewSet(ModelViewSet):

    serializer_class = BlogModelSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
       return Blog.objects.filter(created_by=self.request.user)
    
    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)


class ChatThreadModelViewSet(ModelViewSet):
    queryset = ChatThread.objects.all()
    serializer_class = ChatThreadModelSerializer 
    permission_classes = [IsAuthenticated, ] 

  
class ChatMessageModelViewSet(ModelViewSet):
    queryset = ChatMessage.objects.all()
    serializer_class = ChatMessageModelSerializer 
    permission_classes = [IsAuthenticated, ] 

    def get_queryset(self):
        thread_token = self.query_params.get('token', None)
        ChatMessage.objects.filter(thread_token=thread_token)

    
class CustomerChatRequestModelViewSet(ModelViewSet):
    queryset = CustomerChatRequest.objects.all()
    serializer_class = CustomerChatRequestModelSerializer
    http_method_names = ['post', 'put', 'patch', 'delete', 'head', 'options']
    permission_classes = [IsAuthenticated, ] 

    
class CustomerChatRequestTableModelSerializer(ModelViewSet):   
    queryset = CustomerChatRequest.objects.all()
    serializer_class = CustomerChatRequestTableModelSerializer
    http_method_names = ['get', 'head', 'options']
    permission_classes = [IsAuthenticated,]
