# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.serializers import (ModelSerializer, SerializerMethodField, ValidationError)
from meghauth.models import VendorUser, CustomerUser
from meghvendor.models import VendorProfile
from general.models import (Discount, Ticket, VendorReview, Blog)
from meghauth.models import MeghSvanaUser
from api.general.serializers import BlogModelSerializer
from api.auth.serializers import (PreviewMeghSvanaUserModelSerializer,CustomerUserModelSerializer)
from meghchatserver.models import ChatMessage,ChatThread, CustomerChatRequest

class DiscountModelSerializer(ModelSerializer):

    class Meta:
        model = Discount
        fields = '__all__'


class TicketModelSerializer(ModelSerializer):

    class Meta:
        model = Ticket
        fields = '__all__'


class VendorReviewTableModelSerializer(ModelSerializer):
    created_by = PreviewMeghSvanaUserModelSerializer(read_only=True)
    
    class Meta:
        model = VendorReview
        fields = '__all__'


class BlogModelSerializer(ModelSerializer):
   
    class Meta:
        model = Blog
        fields = '__all__'
class ChatThreadModelSerializer(ModelSerializer):
    class Meta:
        model = ChatThread
        fields = '__all__'  
class ChatMessageModelSerializer(ModelSerializer):
    class Meta:
        model = ChatMessage
        fields = '__all__' 
class CustomerChatRequestModelSerializer(ModelSerializer):
    class Meta:
        model = CustomerChatRequest
        fields = '__all__'

class CustomerUserModelSerializer(ModelSerializer): 
    class Meta:
        model = CustomerUser
        fields = ('id','profile_picture', 'email', 'full_name')   
class CustomerChatRequestTableModelSerializer(ModelSerializer):
    customer = CustomerUserModelSerializer(read_only=True)
    class Meta:
        model = CustomerChatRequest
        fields = '__all__'
