# -*- coding: utf-8 -*-
from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import url
from api.vendor import views
from django.conf.urls import include


router = DefaultRouter()


router.register('discounts', views.DiscountModelViewSet, basename='discounts')
router.register('tickets', views.TicketModelViewSet, basename='tickets')
router.register('vendor-review-table', views.VendorReviewTableModelViewSet, basename='vendor-review-table')
router.register('blog', views.BlogModelViewSet, basename='blog')
router.register('chat-thread', views.BlogModelViewSet, basename='chat-thread')
router.register('chat-messages', views.BlogModelViewSet, basename='chat-messages')
router.register('customer-chat-requests', views.BlogModelViewSet, basename='customer-chat-requests')

urlpatterns = []

urlpatterns += router.urls
