# -*- coding: utf-8 -*-

from rest_framework.serializers import (ModelSerializer, SerializerMethodField, ValidationError)
from meghauth.models import (MeghSvanaUser, VendorUser,
                             CustomerUser, AdminUser)


class VendorUserModelSerializer(ModelSerializer):

    class Meta:
        model = VendorUser
        exclude = ('password',)


class CustomerUserModelSerializer(ModelSerializer):

    class Meta:
        model = CustomerUser
        exclude = ('password',)


class MeghSvanaUserModelSerializer(ModelSerializer):

    class Meta:
        model = MeghSvanaUser
        exclude = ('password',)


class AdminUserModelSerializer(ModelSerializer):

    class Meta:
        model = AdminUser
        exclude = ('password',)


class PreviewMeghSvanaUserModelSerializer(ModelSerializer):

    class Meta:
        model = MeghSvanaUser
        fields = ('id', 'profile_picture', 'email', 'full_name', 'role', 'username', 'country')


class PreviewVendorUserModelSerializer(ModelSerializer):

    class Meta:
        model = VendorUser
        fields = ('id', 'profile_picture', 'email', 'full_name', 'username', 'country')
