# -*- coding: utf-8 -*-

from rest_framework import status
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response
from meghauth.models import (MeghSvanaUser,
                             VendorUser,
                             CustomerUser,
                             AdminUser)
from api.auth.serializers import (VendorUserModelSerializer, CustomerUserModelSerializer,
                                  MeghSvanaUserModelSerializer,
                                  AdminUserModelSerializer)
from api.permissions import (IsSuperUser, IsVendorUser, IsCustomerUser)


class MeghSvanaUserModelViewSet(ModelViewSet):

    queryset = MeghSvanaUser.objects.all()
    serializer_class = MeghSvanaUserModelSerializer
    permission_classes = [IsSuperUser, ]

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)    

    
class VendorUserModelViewSet(ModelViewSet):

    queryset = VendorUser.objects.all()
    serializer_class = VendorUserModelSerializer
    permission_classes = [IsAuthenticated, ]

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)


class CustomerUserModelViewSet(ModelViewSet):

    queryset = CustomerUser.objects.all()
    serializer_class = CustomerUserModelSerializer
    permission_classes = [IsAuthenticated, ]

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)


class AdminUserModelViewSet(ModelViewSet):

    queryset = AdminUser.objects.all()
    serializer_class = AdminUserModelSerializer
    permission_classes = [IsAuthenticated, ]

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)


class CurrentUserApiView(APIView):

    permission_classes = [IsAuthenticated, ]
    serializer_class = MeghSvanaUserModelSerializer
    
    def get(self, request):
        try:
            MeghSvanaUser.objects.get(id=request.user.id)
            return Response(self.serializer_class(MeghSvanaUser.objects.get(id=request.user.id)).data, status=status.HTTP_200_OK)
        except MeghSvanaUser.DoesNotExist:
            return Response({'error': 'not found'}, status=status.HTTP_404_NOT_FOUND)
