# -*- coding: utf-8 -*-

from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import url
from api.auth import views

router = DefaultRouter()

router.register('megh-svana-users', views.MeghSvanaUserModelViewSet, basename='megh-svana-users')
router.register('vendors', views.VendorUserModelViewSet, basename='vendors')
router.register('customers', views.CustomerUserModelViewSet, basename='customers')
router.register('admins', views.AdminUserModelViewSet, basename='admins')

urlpatterns = [
    path('current-user-details/', views.CurrentUserApiView.as_view(), name='current-user-details')
]

urlpatterns += router.urls

