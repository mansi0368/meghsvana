# -*- coding: utf-8 -*-

from rest_framework.routers import DefaultRouter
from rest_framework.permissions import (AllowAny,
                                        IsAuthenticated)
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from django.urls import path, include
from django.conf.urls import url
from api.auth import views
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
    openapi.Info(
        title="MeghSvana API",
        default_version='v1',
    ),
    public=True,
    permission_classes=(IsAuthenticated,),
)

urlpatterns = [
    path('auth/', include('api.auth.urls'), name='auth'),
    path('general/', include('api.general.urls'), name='general'),
     path('website/', include('api.website.urls'), name='website'),
    path('vendor/', include('api.vendor.urls'), name='vendor'),
    path('admin/', include('api.admin.urls'), name='admin'),
    #path('customer/', include('api.customer.urls'), name='customer'),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

