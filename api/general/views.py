# -*- coding: utf-8 -*-

from rest_framework import views, viewsets, status
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from api.general.serializers import (BlogModelSerializer, BlogListModelSerializer,
                                     BlogCategoryModelSerializer, BlogCategoryTableModelSerializer, BlogCategorySelectModelSerializer,
                                     BlogTopicModelSerializer, 
                                     TicketModelSerializer, DiscountModelSerializer,
                                     CampaignModelSerializer, NotifictionModelSerializer,
                                     VendorReviewModelSerializer,
                                     VendorReviewReplyModelSerializer,
                                     InvoiceModelSerializer,
                                     InvoiceItemModelSerializer)
from general.models import (Blog, BlogCategory, BlogTopic, 
                            Ticket, Discount, Campaign, Notification, VendorReview, VendorReviewReply, Invoice, InvoiceItem)
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated
from django.db.models.query_utils import Q



class BlogCategoryTableModelViewSet(ModelViewSet):

    queryset = BlogCategory.objects.all()
    serializer_class = BlogCategoryTableModelSerializer
    permission_classses = [IsAuthenticated, ]
    http_method_names = ['get', 'head', 'options']
    pagination_class = PageNumberPagination
    
    def get_queryset(self):
        query = self.request.query_params.get('search', None)

        if query:
            return self.queryset.filter(Q(name__icontains=query) | Q(slug__icontains=query))
        else:
            return self.queryset


class BlogCategorySelectModelViewSet(ModelViewSet):

    queryset = BlogCategory.objects.all()
    serializer_class = BlogCategorySelectModelSerializer
    permission_classes = [IsAuthenticated, ]
    http_method_names = ['get', 'head', 'options']

    def get_queryset(self):
        query = self.request.query_params.get('search', None)

        if query:
            return self.queryset.filter(name__icontains=query)
        else:
            return self.queryset


class BlogCategoryModelViewSet(ModelViewSet):
    
    queryset = BlogCategory.objects.all()
    serializer_class = BlogCategoryModelSerializer
    permission_classes = [IsAuthenticated, ]
    
    
    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)

    def list(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class BlogTopicModelViewSet(ModelViewSet):

    queryset = BlogTopic.objects.all()
    serializer_class = BlogTopicModelSerializer
    permission_classes = [IsAuthenticated,]   

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user) 


class BlogListModelViewSet(ModelViewSet):

    queryset = Blog.objects.all()
    serializer_class = BlogListModelSerializer
    permission_classes = [IsAuthenticated, ]
    http_method_names = ['get', 'head', 'options']
    pagination_class = PageNumberPagination

    def get_queryset(self):
        query = self.request.query_params.get('search', None)

        if query:
            return self.queryset.filter(Q(title__icontains=query) | Q(category__name__icontains=query))
        else:
            return self.queryset


class BlogModelViewSet(ModelViewSet):

    queryset = Blog.objects.all()
    serializer_class = BlogModelSerializer
    permission_classes = [IsAuthenticated, ]
    
    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)

    def list(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class VendorReviewModelViewSet(ModelViewSet):

    queryset = VendorReview.objects.all()
    serializer_class = VendorReviewModelSerializer
    permission_classes = [IsAuthenticated,]   

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)


class VendorReviewReplyModelViewSet(ModelViewSet):
    
    queryset = VendorReviewReply.objects.all()
    serializer_class = VendorReviewReplyModelSerializer
    permission_classes = [IsAuthenticated,]

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)

## ------------------------------------------------------------------------------------------------------------------ ##

class DiscountModelViewSet(ModelViewSet):
    queryset = Discount.objects.all()
    serializer_class = DiscountModelSerializer
    permission_classes = [IsAuthenticated, ]   

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)


class TicketModelViewSet(ModelViewSet):
    queryset = Ticket.objects.all()
    serializer_class = TicketModelSerializer
    permission_classes = [IsAuthenticated,]   

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)  

# class TaskModelViewSet(ModelViewSet):
#     queryset = Task.objects.all()
#     serializer_class = TaskModelSerializer
#     permission_classes = [IsAuthenticated,]   

#     def perform_create(self, serializer):
#         serializer.save(created_by=self.request.user)

#     def perform_update(self, serializer):
#         serializer.save(updated_by=self.request.user)
class CampaignModelViewSet(ModelViewSet):
    queryset = Campaign.objects.all()
    serializer_class = CampaignModelSerializer
    permission_classes = [IsAuthenticated,]   

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user) 


class NotificationModelViewSet(ModelViewSet):
    queryset = Notification.objects.all()
    serializer_class = NotifictionModelSerializer
    permission_classes = [IsAuthenticated,]   

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user) 
class InvoiceModelViewSet(ModelViewSet):

    queryset = Invoice.objects.all()
    serializer_class = InvoiceModelSerializer
    permission_classes = [IsAuthenticated, ]
    http_method_names = ['get', 'head', 'options']
    pagination_class = PageNumberPagination

    def get_queryset(self):
        query = self.request.query_params.get('search', None)

        if query:
            return self.queryset.filter(Q(invoice__number__icontains=query) | Q(invoice__name__icontains=query) | Q(invoice_from__email__icontains=query))
        else:
            return self.queryset
class InvoiceItemModelViewSet(ModelViewSet):

    queryset = Invoice.objects.all()
    serializer_class = InvoiceItemModelSerializer
    permission_classes = [IsAuthenticated, ]
    http_method_names = ['get', 'head', 'options']
    pagination_class = PageNumberPagination

    def get_queryset(self):
        query = self.request.query_params.get('search', None)

        if query:
            return self.queryset.filter(Q(invoice__icontains=query) | Q(description__icontains=query) | Q(invoice_from__email__icontains=query))
        else:
            return self.queryset                                
