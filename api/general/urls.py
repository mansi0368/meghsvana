from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import url
from api.general import views
from django.conf.urls import include


router = DefaultRouter()

router.register('crud-blog', views.BlogModelViewSet, basename='crud-blog')
router.register('blog-list', views.BlogListModelViewSet, basename='blog-list')
router.register('blog-category-table', views.BlogCategoryTableModelViewSet, basename='blog-category-table')
router.register('blog-category-select', views.BlogCategorySelectModelViewSet, basename='blog-category-select')
router.register('crud-blog-category', views.BlogCategoryModelViewSet, basename='crud-blog-category')
router.register('blog-topics', views.BlogTopicModelViewSet, basename='blog-topics')
router.register('discounts', views.DiscountModelViewSet, basename='discounts')
router.register('tickets', views.TicketModelViewSet, basename='tickets')
router.register('campaigns', views.CampaignModelViewSet, basename='campaigns')
router.register('notifications', views.NotificationModelViewSet, basename='notifications')
router.register('vendor-reviews', views.VendorReviewModelViewSet, basename='vendor-reviews')
router.register('vendor-review-replies', views.VendorReviewReplyModelViewSet, basename='vendor-review-replies')
router.register('invoices', views.InvoiceModelViewSet, basename='invoices')
router.register('invoice-items', views.InvoiceItemModelViewSet, basename='invoice-items')


urlpatterns = []

urlpatterns += router.urls
