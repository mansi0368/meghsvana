# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.serializers import (ModelSerializer, SerializerMethodField, ValidationError)
from general.models import (Blog, BlogCategory, BlogTopic, Discount, Ticket, Campaign, Notification,
                            VendorReview, VendorReviewReply, Invoice, InvoiceItem)
#from api.auth.serializers import (PreviewMeghSvanaUserModelSerializer,)


class BlogCategorySelectModelSerializer(ModelSerializer):
    value = SerializerMethodField()
    text = SerializerMethodField()
    parent = SerializerMethodField()
    
    def get_value(self, instance):
        return instance.id

    def get_text(self, instance):
        return instance.name

    def get_parent(self, instance):
        if instance.parent:
            return instance.parent
        else:
            return None

    class Meta:
        model = BlogCategory
        fields = ('value', 'text', 'parent')

    
class BlogCategoryTableModelSerializer(ModelSerializer):
    posts = SerializerMethodField()

    def get_posts(self, instance):
        if instance.blogs.exists():
            return instance.blogs.all().count()
        else:
            return None

    class Meta:
        model = BlogCategory
        fields = ('id', 'posts', 'name', 'slug')

    
class BlogCategoryModelSerializer(ModelSerializer):
    
    class Meta:
        model = BlogCategory
        fields = '__all__'


class BlogListModelSerializer(ModelSerializer):
    published_by = SerializerMethodField()
    category = SerializerMethodField()

    def get_category(self, instance):
        return instance.category.name
    
    def get_published_by(self, instance):
        return instance.created_by.full_name
    
    class Meta:
        model = Blog
        fields = ('id', 'cover', 'title', 'category', 'status', 'published_by', 'created_at')
        

class BlogModelSerializer(ModelSerializer):
    select_category = SerializerMethodField()
    
    def get_select_category(self, instance):
        return BlogCategorySelectModelSerializer(instance.category).data
    
    class Meta:
        model = Blog
        fields = '__all__'


class BlogTopicModelSerializer(ModelSerializer):

    class Meta:
        model = BlogTopic
        fields = '__all__'  


class VendorReviewModelSerializer(ModelSerializer):

    class Meta:
        model = VendorReview
        fields = '__all__'


class VendorReviewReplyModelSerializer(ModelSerializer):

    class Meta:
        model = VendorReviewReply
        fields = '__all__'

## ----------------------------------------------------------------------------------------------------------------------- ##

class DiscountModelSerializer(ModelSerializer):
    class Meta:
        model = Discount
        fields = '__all__'  

class TicketModelSerializer(ModelSerializer):
    class Meta:
        model = Ticket
        fields = '__all__' 

# class TaskModelSerializer(ModelSerializer):
#     class Meta:
#         model = Task
#         fields = '__all__'                   
class CampaignModelSerializer(ModelSerializer):
    class Meta:
        model = Campaign
        fields = '__all__'  

class NotifictionModelSerializer(ModelSerializer):
    class Meta:
        model = Notification
        fields = '__all__'     
class InvoiceModelSerializer(ModelSerializer):
    class Meta:
        model = Invoice
        fields = '__all__'     
class InvoiceItemModelSerializer(ModelSerializer):
    class Meta:
        model = InvoiceItem
        fields = '__all__'                                                                     
