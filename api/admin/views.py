# -*- coding: utf-8 -*-

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import (AllowAny, IsAdminUser, IsAuthenticated)
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from meghauth.models import (VendorUser, MeghSvanaUser)
from meghvendor.models import (VendorProfile, VendorCategory)
from meghad.models import (Notice, Payroll)
from api.auth.serializers import (VendorUserModelSerializer)
from api.admin.serializers import (VendorModelAddVendorFormStep1Serializer,
                                   VendorCategoryModelSelect2Serializer,
                                   VendorCategoryModelSerializer,
                                   MeghSvanaUserAddFormStep1Serializer,
                                   VendorProfileModelSerializer,
                                   VendorTableModelSerializer,
                                   MeghSvanaUserTableModelSerializer,
                                   VendorCategoryTableModelSerializer,
                                   ValidateVendorProfileDataModelSerializer,
                                   NoticeModelSerializer,
                                   PayrollModelSerializer)
from django.db.models.query_utils import Q

class VendorProfileModelViewSet(ModelViewSet):

    queryset = VendorProfile.objects.all()
    serializer_class = VendorProfileModelSerializer
    permission_classes = [IsAuthenticated, ]
    http_method_names = ['get', 'put', 'patch', 'head', 'options']

    def list(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

        
class VendorTableModelViewSet(ModelViewSet):

    queryset = VendorProfile.objects.all()
    serializer_class = VendorTableModelSerializer
    http_method_names = ['get', 'options', 'head']
    pagination_class = PageNumberPagination
    permission_classes = [IsAuthenticated, ]
    
    def get_queryset(self):
        query = self.request.query_params.get('query[search]', None)
        if query:
            return self.queryset.filter(Q(vendor__email__icontains=query) | Q(vendor__full_name__icontains=query))
        else:
            return VendorProfile.objects.all()


class VendorCategoryTableModelViewSet(ModelViewSet):

    queryset = VendorCategory.objects.all()
    serializer_class = VendorCategoryTableModelSerializer
    http_method_names = ['get', 'options', 'head']
    pagination_class = PageNumberPagination
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        query = self.request.query_params.get('query[search]', None)

        if query:
            return self.queryset.filter(Q(title__icontains=query) | Q(slug__icontains=query))
        else:
            return VendorCategory.objects.all()


class MeghSvanaUserTableModelViewSet(ModelViewSet):

    queryset = MeghSvanaUser.objects.all()
    serializer_class = MeghSvanaUserTableModelSerializer
    http_method_names = ['get', 'options', 'head']
    pagination_class = PageNumberPagination
    permission_classes = [IsAuthenticated,]
    
    def get_queryset(self):
        query = self.request.query_params.get('search', None)

        if query:
            return self.queryset.exclude(id=self.request.user.id).filter(Q(email__icontains=query) | Q(full_name__icontains=query))
        else:
            return self.queryset.exclude(id=self.request.user.id)


class MeghSvanaUserModelViewSet(ModelViewSet):

    queryset = MeghSvanaUser.objects.all()
    serializer_class = MeghSvanaUserAddFormStep1Serializer
    permission_classes = [IsAuthenticated,]
    http_method_names = ['put', 'post', 'patch', 'head', 'options']


class VendorUserModelViewSet(ModelViewSet):

    queryset = VendorUser.objects.all()
    serializer_class = VendorModelAddVendorFormStep1Serializer
    permission_classes = [IsAuthenticated,]
    http_method_names = ['post', 'put', 'patch', 'head', 'options']

    def destroy(self, request, *args, **kwargs):
        try:
            obj = VendorUser.objects.get(id=kwargs.get('pk'))
            obj.is_active = False
            obj.save()
            return Response({'deleted': True}, status=status.HTTP_200_OK)
        except VendorUser.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


class VendorCategoryModelViewSet(ModelViewSet):

    queryset = VendorCategory.objects.all()
    serializer_class = VendorCategoryModelSerializer
    permission_classes = [IsAuthenticated,]
    http_method_names = ['put', 'post', 'patch', 'head', 'options', 'get']

    def list(self, request, *args, **kwargs):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


class NoticeModelViewSet(ModelViewSet):

    queryset = Notice.objects.all()
    serializer_class = NoticeModelSerializer
    permission_classes = [IsAuthenticated,]
    http_method_names = ['put', 'post', 'patch', 'head', 'options', 'get']

    
class PayrollModelViewSet(ModelViewSet):

    queryset = Payroll.objects.all()
    serializer_class = PayrollModelSerializer
    permission_classes = [IsAuthenticated,]
    http_method_names = ['put', 'post', 'patch', 'head', 'options', 'get']


class VendorCategorySelect2ModelViewSet(ModelViewSet):

    queryset = VendorCategory.objects.all()
    serializer_class = VendorCategoryModelSelect2Serializer
    permission_classes = [IsAuthenticated,]

    def list(self, request, *args, **kwargs):
        query = self.request.query_params.get('search', None)

        if query:
            data = self.serializer_class(VendorCategory.objects.filter(Q(title__icontains=query) |
                                                                       Q(slug__icontains=query) |
                                                                       Q(description__icontains=query)), many=True).data
            return Response({'results': data}, status=status.HTTP_200_OK)
        else:
            data = self.serializer_class(self.queryset, many=True).data
            return Response({'results': data}, status=status.HTTP_200_OK)


class CreateVendorHTMLStepFormAPIView(APIView):

    permission_classes = [IsAuthenticated,]

    def post(self, request):
        vendor_user_serializer = VendorModelAddVendorFormStep1Serializer(data=request.data)
        if not vendor_user_serializer.is_valid():
            return Response(vendor_user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            validate_profile_data = ValidateVendorProfileDataModelSerializer(data=self.request.data)
            if validate_profile_data.is_valid():
                obj = vendor_user_serializer.save()
                VendorProfile.objects.filter(vendor=obj).update(**validate_profile_data.validated_data)
                return Response({'created': True}, status=status.HTTP_201_CREATED)
            else:
                return Response(validate_profile_data.errors, status=status.HTTP_400_BAD_REQUEST)
