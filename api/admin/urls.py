# -*- coding: utf-8 -*-

from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import url
from api.admin import views

router = DefaultRouter()

router.register('vendor-table', views.VendorTableModelViewSet, basename='vendor-table')
router.register('crud-vendor-category', views.VendorCategoryModelViewSet, basename='crud-vendor-category')
router.register('vendor-category-table', views.VendorCategoryTableModelViewSet, basename='vendor-category-table')
router.register('meghsvana-user-table', views.MeghSvanaUserTableModelViewSet, basename='meghsvana-user-table')
router.register('rud-vendor-profile', views.VendorProfileModelViewSet, basename='rud-vendor-profile')
router.register('cu-vendor-users', views.VendorUserModelViewSet, basename='cu-vendor-users')
router.register('cu-meghsvana-users', views.MeghSvanaUserModelViewSet, basename='cu-meghsvana-users')
router.register('notice', views.NoticeModelViewSet, basename='notice')
router.register('payroll', views.PayrollModelViewSet, basename='payroll')
router.register('vendor-categories-select2', views.VendorCategorySelect2ModelViewSet, basename='vendor-categories-select2')

urlpatterns = [
    path('add-vendor/', views.CreateVendorHTMLStepFormAPIView.as_view(), name='add-vendor')
]

urlpatterns += router.urls
