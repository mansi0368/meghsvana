# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.serializers import (ModelSerializer, SerializerMethodField, ValidationError)
from rest_framework.validators import UniqueValidator
from meghauth.models import (VendorUser, MeghSvanaUser)
from meghvendor.models import (VendorProfile, VendorCategory, VendorReportSetting)
from meghad.models import (VendorServicePermission, Notice, Payroll)
from general.models import (VendorReview,)
from api.auth.serializers import (PreviewMeghSvanaUserModelSerializer,)


class VendorModelAddVendorFormStep1Serializer(ModelSerializer):

    class Meta:
        model = VendorUser
        fields = ('id', 'email', 'full_name', 'username', 'dob', 'country', 'country_code', 'p_phone', 's_phone', 'about', 'profile_picture')


class MeghSvanaUserAddFormStep1Serializer(ModelSerializer):

    class Meta:
        model = MeghSvanaUser
        fields = ('id','email', 'full_name', 'username', 'dob', 'country', 'role', 'country_code', 'p_phone', 's_phone', 'about')


class VendorServicePermissionModelSerializer(ModelSerializer):

    class Meta:
        model = VendorServicePermission
        fields = ('is_vc', 'is_ac', 'is_ch', 'is_ls', 'is_tr', 'is_vr', 'vendor')


class VendorReportSettingModelSerializer(ModelSerializer):

    class Meta:
        model = VendorReportSetting
        fields = ('id', 'vendor', 'category', 'report_medium', 'video_usd_rate', 'video_inr_rate', 'text_usd_rate', 'text_inr_rate')


class VendorReviewModelSerializer(ModelSerializer):
    created_by = PreviewMeghSvanaUserModelSerializer(read_only=True)
    
    class Meta:
        model = VendorReview
        fields = ('id', 'review', 'rating', 'created_by')


class VendorProfileModelSerializer(ModelSerializer):
    global_permissions = SerializerMethodField()
    report_settings = SerializerMethodField()
    vendor_user = SerializerMethodField()
    reviews = SerializerMethodField()
    order_count = SerializerMethodField()
    revenue = SerializerMethodField()
    aa_access_marker = SerializerMethodField()
    rating_point = SerializerMethodField()
    
    def get_rating_point(self, instance):
        rating = 0
        if instance.rating:
            return instance.rating
        else:
            return rating
    
    def get_aa_access_marker(self, instance):
        if instance.aa_access == True:
            return 'Allowed'
        else:
            return 'Not Allowed'
    
    def get_revenue(self, instance):
        count = 0
        return count 
    
    def get_order_count(self, instance):
        return instance.vendor.vendor_customerchatrequests.count()
    
    def get_reviews(self, instance):
        return VendorReviewModelSerializer(instance.vendor.vendorreviews.all(), many=True).data
    
    def get_vendor_user(self, instance):
        return VendorModelAddVendorFormStep1Serializer(instance.vendor).data
    
    def get_report_settings(self, instance):
        return VendorReportSettingModelSerializer(instance.vendor.vendor_report_settings.all(), many=True).data
    
    def get_global_permissions(self, instance):
        return VendorServicePermissionModelSerializer(instance.vendor.vendorservicepermission).data
    
    class Meta:
        model = VendorProfile
        exclude = ('unapproved_fields',)


class VendorTableModelSerializer(ModelSerializer):
    
    category = SerializerMethodField()
    usd_rate = SerializerMethodField()
    inr_rate = SerializerMethodField()
    vendor = VendorModelAddVendorFormStep1Serializer(read_only=True)
    status = SerializerMethodField()

    def get_status(self, instance):
        if instance.status == 'ON':
            return 'O'
        else:
            return 'F'
    
    def get_category(self, instance):
        if instance.category:
            return instance.category.title
        else:
            return None

    def get_usd_rate(self, instance):
        if instance.usd_rate:
            if instance.usd_rate_type == 'PM':
                return '{rate}/m'.format(rate=instance.usd_rate)
            else:
                return '{rate}/h'.format(rate=instance.usd_rate)
        else:
            return None

    def get_inr_rate(self, instance):
        if instance.inr_rate:
            if instance.inr_rate_type == 'PM':
                return '{rate}/m'.format(rate=instance.inr_rate)
            else:
                return '{rate}/h'.format(rate=instance.inr_rate)
        else:
            return None
    
    class Meta:
        model = VendorProfile
        fields = ('id', 'category', 'usd_rate', 'inr_rate', 'vendor', 'status') 


class VendorCategoryTableModelSerializer(ModelSerializer):
    vendors_count = SerializerMethodField()

    def get_vendors_count(self, instance):
        return instance.vendorprofiles.all().count()

    class Meta:
        model = VendorCategory
        fields = ('id', 'title', 'slug', 'vendors_count')


class MeghSvanaUserTableModelSerializer(ModelSerializer):
    role = SerializerMethodField()
    status = SerializerMethodField()

    def get_status(self, instance):
        return instance.is_active
    
    def get_role(self, instance):
        ROLE_MAPPING = {
            'V': 'Vendor',
            'CS': 'Customer',
            'A': 'Admin',
        }
        return ROLE_MAPPING.get(instance.role, 'Not Assigned')

    class Meta:
        model = MeghSvanaUser
        fields = ('id', 'email', 'full_name', 'role', 'status') 


class VendorCategoryModelSerializer(ModelSerializer):

    class Meta:
        model = VendorCategory
        fields = '__all__'


class NoticeModelSerializer(ModelSerializer):
    class Meta:
        model = Notice
        fields = '__all__'  


class PayrollModelSerializer(ModelSerializer):
    class Meta:
        model = Payroll
        fields = '__all__'


class VendorCategoryModelSelect2Serializer(ModelSerializer):

    text = SerializerMethodField()

    def get_text(self, instance):
        return instance.title

    class Meta:
        model = VendorCategory
        fields = ('id', 'text')


class ValidateVendorProfileDataModelSerializer(ModelSerializer):

    class Meta:
        model = VendorProfile
        exclude = ('vendor',)
