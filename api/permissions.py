# -*- coding: utf-8 -*-
"""
Custom Permission classes 
"""

from rest_framework.permissions import BasePermission


class IsSuperUser(BasePermission):
    """                                                                                                                                                                                                       
    Check if the user associated with the request                                                                                                                                                                 
    is "Super User" with it along with                                                                                                                                                      
    that few conditions need to be checked.                                                                                                                                                                       
    : user is_active flag should be True                                                                                                                                                                          
    : user should be authenticated                                                                                                                                                                                
    """
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated and request.user.is_active and request.user.is_superuser)


class IsVendorUser(BasePermission):
    """
    Check if the user associated with the request                                                                                                                                                                 
    is "Vendor User" with it along with                                                                                                                                                      
    that few conditions need to be checked.                                                                                                                                                                       
    : user is_active flag should be True                                                                                                                                                                          
    : user should be authenticated
    """
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated and request.user.is_active and request.user.role == 'V')


class IsCustomerUser(BasePermission):
    """
    Check if the user associated with the request                                                                                                                                                                 
    is "Custom User" with it along with                                                                                                                                                      
    that few conditions need to be checked.                                                                                                                                                                       
    : user is_active flag should be True                                                                                                                                                                          
    : user should be authenticated
    """
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated and request.user.is_active and request.user.role == 'CS')
