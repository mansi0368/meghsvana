# -*- coding: utf-8 -*-

from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import url
from api.website import views

router = DefaultRouter()

router.register('vendor-card-list', views.WebsiteVendorCardListModelViewSet, basename='vendor-card-list')
router.register('vendor-review', views.WebsiteVendorReviewsModelViewSet, basename='vendor-review')
router.register('order', views.OrderModelViewSet, basename='order')


urlpatterns = []
urlpatterns += router.urls
