# -*- coding: utf-8 -*-

from rest_framework import status
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import (AllowAny, IsAdminUser, IsAuthenticated)
from rest_framework.pagination import LimitOffsetPagination
from meghvendor.models import (VendorProfile,)
from api.website.serializers import (WebsiteVendorCardListModelSerializer,OrderModelSerializer, WebsiteVendorReviewModelSerializer, WebsiteVendorReplyModelSerializer)
from general.models import (VendorReview,VendorReviewReply,VendorUser)
from meghcustomer.models import Order

# Create your views here.

class WebsiteVendorCardListModelViewSet(ModelViewSet):

    queryset = VendorProfile.objects.filter(vendor__is_active=True)
    serializer_class = WebsiteVendorCardListModelSerializer
    permission_classes = [IsAuthenticated,]
    http_method_names = ['get', 'head', 'options']


class WebsiteVendorReviewsModelViewSet(ModelViewSet):

    queryset = VendorReview.objects.filter(vendor__is_active=True)
    serializer_class = WebsiteVendorReviewModelSerializer
    permission_classes = [IsAuthenticated,]
    http_method_names = ['get', 'head', 'options']

    def get_queryset(self):
        return self.queryset.filter(vendor=self.request.query_params.get('vendor'))

class WebsiteVendorReplyModelViewSet(ModelViewSet):

    queryset = VendorReviewReply.objects.all()
    serializer_class = WebsiteVendorReplyModelSerializer
    permission_classes = [IsAuthenticated,]
    http_method_names = ['post', 'put', 'patch', 'delete', 'head', 'options']
   
    def get_queryset(self):
        return self.queryset.filter(vendor_review=self.query_params.get('vendor_review'))


class OrderModelViewSet(ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderModelSerializer
    permission_classes = [IsAuthenticated,]
    http_method_names = ['post', 'put', 'patch', 'delete', 'head', 'options']
