# -*- coding: utf-8 -*-

from rest_framework import status
from rest_framework.serializers import (ModelSerializer, SerializerMethodField)
from meghauth.models import (VendorUser,)
from meghvendor.models import (VendorProfile,)
from api.auth.serializers import (PreviewVendorUserModelSerializer, PreviewMeghSvanaUserModelSerializer)
from general.models import (VendorReview, VendorReviewReply)
from meghcustomer.models import Order

class WebsiteVendorCardListModelSerializer(ModelSerializer):
    vendor = PreviewVendorUserModelSerializer(read_only=True)
    total_reviews = SerializerMethodField()

    def get_total_reviews(self, instance):
        return instance.vendorreviews.count()
    
    class Meta:
        model = VendorProfile
        fields = ('id', 'vendor', 'category', 'skills', 'languages', 'usd_rate', 'total_reviews', 'experience',
                  'inr_rate', 'usd_rate_type', 'inr_rate_type', 'usd_rate', 'inr_rate')


class WebsiteVendorReplyModelSerializer(ModelSerializer):
    created_by = PreviewMeghSvanaUserModelSerializer(read_only=True)
    
    class Meta:
        model = VendorReviewReply
        fields = ('id', 'reply', 'vendor_review', 'created_at', 'created_by')


class WebsiteVendorReviewModelSerializer(ModelSerializer):
    vendor = PreviewVendorUserModelSerializer(read_only=True)
    created_by = PreviewMeghSvanaUserModelSerializer(read_only=True)
    replies = SerializerMethodField()

    def get_replies(self, instance):
        return WebsiteVendorReplyModelSerializer(instance.vendorreviewreplies.all(), many=True).data

    class Meta:
        model = VendorReview
        fields = '__all__'
class OrderModelSerializer(ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'        

