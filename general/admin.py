# -*- coding: utf-8 -*-

from django.contrib import admin
from django.contrib.auth.models  import Group
from general.models import (Blog, BlogCategory, BlogTopic, Discount, Ticket, Campaign, Notification,
                            VendorReview, VendorReviewReply, Invoice, InvoiceItem)


class BlogAdmin(admin.ModelAdmin):

    model = Blog
    fields = ('title', 'category', 'content', 'status', 'cover', 'slug',
              'tags', 'is_public', 'created_at', 'updated_at', 'created_by', 'updated_by')
    list_display = ('id', 'title', 'category', 'status', 'is_public', 'created_at', 'created_by')
    list_filter = ('is_public', )
    readonly_fields = ('created_at', 'updated_at', 'created_by', 'updated_by')


class BlogCategoryAdmin(admin.ModelAdmin):

    model = BlogCategory
    fields  = ('name', 'parent', 'slug', 'description', 'created_at',
               'updated_at', 'created_by', 'updated_by')
    list_display = ('id', 'name', 'parent', 'slug', 'created_at', 'created_by')
    readonly_fields = ('created_at', 'updated_at', 'created_by', 'updated_by') 


class BlogTopicAdmin(admin.ModelAdmin):

    model = BlogTopic
    fields = ('title', 'category', 'due_date', 'description', 'created_at', 'updated_at', 'created_by', 'updated_by')
    list_display = ('id', 'title', 'category', 'due_date', 'description', 'created_at', 'created_by')
    readonly_fields = ('created_at', 'updated_at', 'created_by', 'updated_by') 


class VendorReviewAdmin(admin.ModelAdmin):

    model = VendorReview
    fields = ('review', 'vendor', 'rating', 'created_at', 'updated_at', 'created_by') 
    readonly_fields = ('created_at', 'updated_at', 'created_by') 
    list_display = ('id', 'review', 'vendor', 'rating', 'created_at', 'updated_at', 'created_by')
    

class VendorReviewReplyAdmin(admin.ModelAdmin):

    model = VendorReviewReply
    fields = ('reply', 'vendor_review', 'reply_file', 'created_at', 'updated_at', 'created_by') 
    readonly_fields = ('created_at', 'updated_at', 'created_by')
    list_display = ('id', 'reply', 'created_at', 'updated_at')
    
## - ----------------------------------------------------------------------------------------------------------- - ##

class DiscountAdmin(admin.ModelAdmin):
    fields = ('discounttype', 'discountvalueinr', 'discountvalueusd', 'servicecategory', 'reportcategory',
              'servicemedium', 'existingvendorrate', 'currentvendorrate', 'validon', 'vendorshare', 'adminshare', 'startdate',
              'timing1', 'enddate', 'timing2', 'discountinfo') 
    readonly_fields = ('created_at', 'updated_at', 'created_by', 'updated_by') 

class TicketAdmin(admin.ModelAdmin):
    fields = ('department', 'role', 'employeeid', 'phone', 'issuecategory', 'email', 'priority', 'agent', 
               'issuedescription', 'attachments') 
    readonly_fields = ('created_at', 'updated_at', 'created_by', 'updated_by') 

class CampaignAdmin(admin.ModelAdmin):
    fields = ('title', 'campaigntype', 'size', 'discounttype', 'redemptionlimit',
             'codecount', 'discount', 'startdate', 'timing1', 'validon', 'enddate',
              'timing2', 'customcode') 
    readonly_fields = ('created_at', 'updated_at', 'created_by', 'updated_by')  

class NotificationAdmin(admin.ModelAdmin):
    fields = ('heading', 'subheading', 'picture', 'platform', 'priority',
             'date', 'time', 'target_countries', 'description') 
    readonly_fields = ('created_at', 'updated_at', 'created_by', 'updated_by')   
class InvoiceAdmin(admin.ModelAdmin):
    fields = ('invoice_number', 'invoive_name', 'invoice_from', 'invoice_to', 'subtotal', 'taxes', 'invoice_is_paid') 
    readonly_fields = ('created_at', 'updated_at', 'created_by')                  
class InvoiceItemAdmin(admin.ModelAdmin):
    fields = ('num', 'description', 'quantity', 'price','invoice') 
    readonly_fields = ('created_at', 'updated_at', 'created_by')                  
                               
      
      

# Register your models here.
admin.site.register(Blog, BlogAdmin)
admin.site.register(BlogCategory, BlogCategoryAdmin)
admin.site.register(BlogTopic, BlogTopicAdmin)
admin.site.register(VendorReview, VendorReviewAdmin)
admin.site.register(VendorReviewReply, VendorReviewReplyAdmin)
admin.site.register(Invoice, InvoiceAdmin)
admin.site.register(InvoiceItem, InvoiceItemAdmin)

# admin.site.register(Discount, DiscountAdmin)
# admin.site.register(Ticket, TicketAdmin)
# admin.site.register(Task, TaskAdmin)
# admin.site.register(Campaign, CampaignAdmin)
# admin.site.register(Notification, NotificationAdmin)

