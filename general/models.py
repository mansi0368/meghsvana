# -*- coding: utf-8 -*-

from django.db import models
from meghauth.models import MeghSvanaUser
from django.contrib.postgres.fields import ArrayField
from meghauth.models import VendorUser
from meghad.models import Department

# Create your models here.

class BlogCategory(models.Model):
    
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=200)
    parent = models.ForeignKey('BlogCategory', on_delete=models.SET_NULL, null=True, blank=True)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='created_by_blogcategories')
    updated_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='updated_by_blogcategories')

    def __str__(self):
        return '#ID-%s - %s' % (self.id, self.name)

    
    class Meta:
        verbose_name = "Blog Category"
        verbose_name_plural = "Blog Categories"


class Blog(models.Model):
    
    STATUS_CHOICE = (
        ('PB', 'Published'),
        ('PE', 'Pending'),
        ('D', 'Denied')
    )
    title = models.CharField(max_length=100)
    category = models.ForeignKey(BlogCategory, related_name='blogs', on_delete=models.CASCADE)
    status = models.CharField(choices=STATUS_CHOICE, max_length=50, default='PE')
    content = models.TextField()
    cover = models.ImageField(upload_to='blog/covers/')
    slug = models.SlugField(max_length=200)
    tags = ArrayField(models.CharField(max_length=250), null=True, blank=True)
    is_public = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='created_by_blogs')
    updated_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='updated_by_blogs')              
      

    def __str__(self):
        return '#ID-%s - %s' % (self.id, self.title)
    
    class Meta:
        verbose_name = "Blog"
        verbose_name_plural = "Blogs"


class BlogTopic(models.Model):

    title = models.CharField(max_length=50)
    category = models.ForeignKey(BlogCategory, on_delete=models.CASCADE, related_name='blogtopics') 
    due_date = models.DateField()  
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='created_by_blogtopics')
    updated_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='updated_by_blogtopics')
    
    def __str__(self):
        return '#ID-%s - %s' % (self.id, self.title)
    
    class Meta:
        verbose_name = "Blog Topic"
        verbose_name_plural = "Blog Topics"


class Discount(models.Model):
    
    discounttitle = models.CharField(max_length=100)
    discounttype = models.CharField(max_length=50)
    discountvalueinr = models.IntegerField()
    discountvalueusd = models.IntegerField()
    servicecategory = models.CharField(max_length=50)
    reportcategory = models.CharField(max_length=50)
    servicemedium = models.CharField(max_length=50)
    existingvendorrate = models.IntegerField()
    currentvendorrate = models.IntegerField()
    validon = models.DateField()
    vendorshare = models.IntegerField()
    adminshare = models.IntegerField()
    startdate = models.DateField()
    timing1 = models.TimeField()
    enddate = models.DateField()
    timing2 = models.TimeField()
    discountinfo = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='created_by_discounts')
    updated_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='updated_by_discounts')

    class Meta:
        verbose_name = "Discount"
        verbose_name_plural = "Discounts"


class Ticket(models.Model):
    
    department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name='tickets')
    role = models.CharField(max_length=50)
    employeeid = models.CharField(max_length=50)
    phone = models.IntegerField()
    issuecategory = models.CharField(max_length=50)
    email = models.EmailField()
    priority = models.CharField(max_length=50)
    agent = models.CharField(max_length=50)
    issuedescription = models.TextField()
    attachments = models.FileField()
    due_date = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='created_by_tickets')
    updated_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='updated_by_tickets')

    class Meta:
        verbose_name = "Ticket"
        verbose_name_plural = "Tickets"


class Campaign(models.Model):
    
    title = models.CharField(max_length=100)
    campaigntype = models.CharField(max_length=50)
    size = models.CharField(max_length=50)
    discounttype = models.CharField(max_length=50)
    redemptionlimit = models.CharField(max_length=50)
    codecount = models.IntegerField()
    discount = models.IntegerField()
    startdate = models.DateField()
    timing1 = models.TimeField()
    validon = models.CharField(max_length=50)
    enddate = models.DateField()
    timing2 = models.TimeField()
    customcode = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='created_by_campaigns')
    updated_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='updated_by_campaigns')

    class Meta:
        verbose_name = "Campaign"
        verbose_name_plural = "Campaigns"


class Notification(models.Model):

    heading = models.CharField(max_length=100)
    subheading = models.CharField(max_length=100)
    picture = models.ImageField()
    is_active = models.BooleanField(default=True)
    platform = models.CharField(max_length=75)
    priority = models.CharField(max_length=75)
    date = models.DateField()
    time = models.TimeField()
    target_countries = models.CharField(max_length=75)
    description = models.TextField()
    created_at = models.DateTimeField( auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='created_by_notifications')
    updated_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='updated_by_notifications')

    class Meta:
        verbose_name = "Notification"
        verbose_name_plural = "Notifications"


class VendorReview(models.Model):

    SERVICE_CHOICES = [
        ('VC','Video Call'),
        ('AC','Audio Call'),
        ('CH','Chatting Service'),
        ('LS','Live Service'),
        ('TR','Text Report'),
        ('VR','Video Report'),
    ]
    review = models.CharField(max_length=150)
    vendor = models.ForeignKey(VendorUser, on_delete=models.CASCADE, related_name='vendorreviews')
    service = models.CharField(choices=SERVICE_CHOICES, max_length=50, null=True, blank=True)
    rating = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='created_by_vendorreviews')

    def __str__(self):
        return '#ID-%s - %s' % (self.id, self.review)
    
    class Meta:
        verbose_name = "Vendor Review"
        verbose_name_plural = "Vendor Reviews"  


class VendorReviewReply(models.Model):
    
    reply = models.CharField(max_length=250)
    vendor_review = models.ForeignKey(VendorReview, on_delete=models.CASCADE, related_name='vendorreviewreplies')
    reply_file = ArrayField(models.FileField(upload_to='reply/files/'), null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='created_by_vendorreviewreplies')

    def __str__(self):
        return '#ID-%s - %s' % (self.id, self.reply)
    
    class Meta:
        verbose_name = "Vendor Review Reply"
        verbose_name_plural = "Vendor Review Replies"


class Invoice(models.Model):
    
    invoice_number = models.CharField(max_length=50)
    invoive_name = models.CharField(max_length=50)
    invoice_from = models.ForeignKey(MeghSvanaUser, on_delete=models.CASCADE)
    invoice_to = models.ForeignKey(MeghSvanaUser, on_delete=models.CASCADE, related_name='invoices')
    subtotal = models.IntegerField()  
    taxes = models.IntegerField()
    invoice_is_paid = models.BooleanField() 
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='created_by_invoices')
    class Meta:
        verbose_name = "Invoice"
        verbose_name_plural = "Invoices"


class InvoiceItem(models.Model):
    
    num = models.IntegerField()
    description = models.TextField()
    quantity = models.IntegerField()
    price = models.IntegerField()
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE, related_name='property') 
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey('meghauth.MeghSvanaUser',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True,
                                   related_name='created_by_invoiceitems')

    class Meta:
        verbose_name = "Invoice Item"
        verbose_name_plural = "Invoice Items"
