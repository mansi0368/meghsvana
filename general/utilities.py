# -*- coding: utf-8 -*-

"""
General methods which are redundant in application
"""
FLAG_FIELDS = [
    "profile_picture",
    "vendor",
    "dob",
    "category",
    "country",
    "country_code",
    "p_phone",
    "s_phone",
    "usd_rate",
    "inr_rate",
    "usd_rate_type",
    "inr_rate_type",
    "aa_access",
    "skills",
    "languages",
    "certificates",
    "aadhar_no",
    "aadhar_card",
    "pan_no",
    "pan_card",
    "gst_no",
    "tax_remitence_file",
    "cv",
    "is_vc",
    "is_ac",
    "is_ch",
    "is_ls",
    "is_tr",
    "is_vr",
    "unapproved_fields",
    "intro_video",
    "about"
]

def get_settings(key):
    from django.conf import settings
    return key

def get_querydict(data):
    """
    send a dict to get a query dict
    """
    query_dict = QueryDict('', mutable=True)
    query_dict.update(data)
    return query_dict
