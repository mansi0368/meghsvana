# -*- coding: utf-8 -*-

import os
from celery import Celery

app = Celery('meghsvana')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

@app.task(bind=True)
def debug_task(self):
    """                                                                                                                                                                                                       
    This task is officialy provided by the                                                                                                                                                                        
    Documentatino of celery on Django                                                                                                                                                                             
    https://docs.celeryproject.org/en/latest/django/first-steps-with-django.html                                                                                                                                  
    """
    print("DEBUG TASK BEGIN")
    return 0
