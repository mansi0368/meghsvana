# -*- coding: utf-8 -*-

from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from general.utilities import get_settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('api.urls')),
    path('auth/', include('meghauth.urls')),
    path('', include('website.urls')),
    path('meghad/', include('meghad.urls')),
    path('meghvendor/', include('meghvendor.urls')),
    path('test-chat/', include('meghchatserver.urls'))
]+ static(get_settings('settings.MEDIA_URL'), document_root=get_settings('settings.MEDIA_ROOT'))


# Django Administration Pannel Settings

admin.site.site_header = "MeghSvana Admin"
admin.site.site_title = "MeghSvana Admin Portal"
admin.site.index_title = "Welcome to MeghSvana Admin Portal"

# Disable side-bar func.
admin.site.enable_nav_sidebar = False
