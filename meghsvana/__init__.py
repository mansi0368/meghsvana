# -*- coding: utf-8 -*-

from .celery import app as megh_celery

__all__ = ('megh_celery',)
