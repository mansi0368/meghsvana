function getCookie(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie !== '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = cookies[i].trim();
			// Does this cookie string begin with the name we want?
			if (cookie.substring(0, name.length + 1) === (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}


function VerifyForm(stepno) {
    let select_names = ['country', 'languages', 'skills', 'category', 'rate_type']
    let file_names = ['aadhar_card', 'pan_card', 'tax_remitence_file', 'intro_video', 'certificates']
	$('#kt_form *').filter($('.form-control')).each(function () {
		if (this.name.length != 0) {
			if (select_names.indexOf(this.name) == -1) {
                $('#' + this.name).text(this.value)
            } 
            else {
				let value = $("#" + this.id + " option:selected").text()
				$('#' + this.name).text(value)
            }
            if (file_names.includes(this.name)) {
                if ($("input[name=" + this.name + "]").val()) {
                    console.log('true')
                    $('#check_' + this.name).prop('checked', true)
                } else {
                    console.log('false')
                    $('#check_' + this.name).prop('checked', false)
            }
        }
    }
    });
}

$('.form-control').on('change', function () {
	$("[id^='error']").text('')
	$("[id^='error']").prev().removeClass('text-danger')
})

function CreateVendor(target) {
    event.preventDefault()
	let data = new FormData(target)
	let cookie = getCookie('csrftoken');
	$.ajax({
		url: '/api/v1/admin/add-vendor/',
		type: 'post',
		processData: false,
		contentType: false,
		headers: { 'X-CSRFToken': cookie },
        data: data,
        beforeSend: function () {
            $('#CreateVendorButton').attr('disabled', true);
            $('#CreateVendorButton').addClass('spinner spinner-white spinner-right')
        },
		success: function (data) {
			Swal.fire({
				icon: "success",
				title: "Vendor Created Succesfully",
				showConfirmButton: false,
				timer: 1500
			}).then(function() {
                window.location.reload()
            });
		},
		error: function (data) {
			console.log(data)
			if (data['responseJSON']) {
				let i;
				let keys = Object.keys(data['responseJSON'])
				for (let key of keys) {
					if (data['responseJSON'].non_field_errors) {
						Swal.fire({
							text: data['responseJSON'].non_field_errors[0],
							icon: "warning",
							buttonsStyling: !1,
							confirmButtonText: "Ok, got it!",
							customClass: {
								confirmButton: "btn font-weight-bold btn-light"
							}
						})
					} else {
						$('#error_' + key).text(' ' + data['responseJSON'][key][0])
						$('#' + key).addClass('text-danger')
					}
				}
				if (typeof data['responseJSON'].non_field_errors == "undefined") {
					Swal.fire({
						text: 'Looks like there are some errors, Please check the Review panel of form.',
						icon: "warning",
						buttonsStyling: !1,
						confirmButtonText: "Ok, got it!",
						customClass: {
							confirmButton: "btn font-weight-bold btn-light"
						}
					})
				}
			} else {
				Swal.fire({
					icon: "error",
					title: data['statusText'],
					showConfirmButton: false,
					timer: 1500
				});
            }
            $('#CreateVendorButton').attr('disabled', true);
            $('#CreateVendorButton').addClass('spinner spinner-white spinner-right')
        },
        complete: function () {
            $('#CreateVendorButton').attr('disabled', false);
            $('#CreateVendorButton').removeClass('spinner spinner-white spinner-right')
        }
	})
}

var KTWizard4 = function () {
    var t, e, i, o = [];
    return {
        init: function () {
            t = KTUtil.getById("kt_wizard"),
                e = KTUtil.getById("kt_form"),
                (i = new KTWizard(t, {
                    startStep: 1,
                    clickableSteps: !1
                })).on("change", (function (t) {
                    if (!(t.getStep() > t.getNewStep())) {
                        var e = o[t.getStep() - 1];
                        if (t.getNewStep() == 4) {
                            VerifyForm(t.getNewStep())
                        }
                        return e && e.validate().then((function (e) {
                            "Valid" == e ? (t.goTo(t.getNewStep()),
                                KTUtil.scrollTop()) : Swal.fire({
                                    text: "Please fill your correctly before procceding",
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn font-weight-bold btn-light"
                                    }
                                }).then((function () {
                                    KTUtil.scrollTop()
                                }
                                ))
                        }
                        )),
                            !1
                    }
                }
                )),
                i.on("changed", (function (t) {
                    KTUtil.scrollTop()
                }
                )),
                i.on("submit", (function (t) {
                    CreateVendor($('#kt_form')[0])
                }
                )),
                o.push(FormValidation.formValidation(e, {
                    fields: {
                        full_name: {
                            validators: {
                                notEmpty: {
                                    message: "Full Name is required"
                                },
                                stringLength: {
                                    max: 150,
                                    message: 'The full name must be less than 50 characters'
                                }
                            }
                        },
                        username: {
                            validators: {
                                notEmpty: {
                                    message: "User Name is required"
                                },
                                stringLength: {
                                    max: 150,
                                    message: 'The user name must be less than 150 characters'
                                }
                            }
                        },
                        email: {
                            validators: {
                                notEmpty: {
                                    message: "Primary Email is required"
                                },
                                emailAddress: {
                                    message: 'Not a valid Email Address'
                                },
                                stringLength: {
                                    max: 50,
                                    message: 'The Primary Email must be less than 150 characters'
                                }
                            }
                        },
                        p_phone: {
                            validators: {
                                notEmpty: {
                                    message: "Primary Email is required"
                                },
                                between: {
                                    min: 1000000000,
                                    max: 9999999999,
                                    message: 'Use format 989xxxx907 for Contact Numbers'
                                },
                                stringLength: {
                                    max: 10,
                                    message: 'Phone Numbers must be less than 10 characters'
                                }
                            }
                        },
                        s_phone: {
                            validators: {
                                between: {
                                    min: 1000000000,
                                    max: 9999999999,
                                    message: 'Use format 989xxxx907 for Contact Numbers'
                                },
                                stringLength: {
                                    max: 10,
                                    message: 'Phone Numbers must be less than 10 characters'
                                }
                            }
                        },
                        country: {
                            validators: {
                                notEmpty: {
                                    message: "Country is required"
                                }
                            }
                        },
                        about: {
                            validators: {
                                notEmpty: {
                                    message: "About is required"
                                }
                            }
                        },
                        dob: {
                            validators: {
                                notEmpty: {
                                    message: "Date of Birth is required"
                                }
                            }
                        }
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger,
                        bootstrap: new FormValidation.plugins.Bootstrap({
                            eleValidClass: ""
                        })
                    }
                })),
                o.push(FormValidation.formValidation(e, {
                    fields: {
                        experience: {
                            validators: {
                                notEmpty: {
                                    message: "Experience is required"
                                },
                                between: {
                                    min: 0,
                                    max: 99,
                                    message: 'Expirience should be a numerical value'
                                },
                                stringLength: {
                                    max: 3,
                                    message: 'Expirience should be less than 3 characters'
                                }
                            }
                        },
                        languages: {
                            validators: {
                                notEmpty: {
                                    message: "Provide Languages supported by vendor"
                                }
                            }
                        },
                        skills: {
                            validators: {
                                notEmpty: {
                                    message: "Provide skills for vendor"
                                }
                            }
                        },
                        category: {
                            validators: {
                                notEmpty: {
                                    message: "Category is required for vendor"
                                }
                            }
                        },
                        inr_rate: {
                            validators: {
                                notEmpty: {
                                    message: "INR Rate is required"
                                }
                            }
                        },
                        usd_rate: {
                            validators: {
                                between: {
                                    min: 0,
                                    max: 999,
                                    message: 'Provide USD Rate for Vendor'
                                },
                                stringLength: {
                                    max: 3,
                                    message: 'USD Rate should be less than 3 characters'
                                }
                            }
                        },
                        rate_type: {
                            validators: {
                                notEmpty: {
                                    message: "Rate Type is required"
                                }
                            }
                        }
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger,
                        bootstrap: new FormValidation.plugins.Bootstrap({
                            eleValidClass: ""
                        })
                    }
                })),
                o.push(FormValidation.formValidation(e, {
                    fields: {
                        aadhar_no: {
                            validators: {
                                notEmpty: {
                                    message: "Aadhar No is required"
                                },
                                between: {
                                   min: 100000000000,
                                   max: 999999999999,
                                   message: 'Addhar No should be a numerical value'
                                },
                                stringLength: {
                                    max: 12,
                                    message: 'Addhar No  should be less than 12 characters'
                                }
                            }
                        },
                        pan_no: {
                            validators: {
                                notEmpty: {
                                    message: "PAN No is required"
                                },
                                regexp: {
                                    regexp: /^[A-Z]{5}\d{4}[A-Z]{1}/i,
                                    message: 'Pan No should be in format AAAAA9999B'
                                }
                            }
                        },
                        gst_no: { 
                            validators: {
                                regexp: {
                                    regexp: /^([0]{1}[1-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-7]{1})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/,
                                    message: 'Invalid Gst number format'
                                }
                            }
                        },
                        pan_card: {
                            validators: {
                                notEmpty: {
                                    message: "PAN Card is required"
                                }
                            }
                        },
                        aadhar_card: {
                            validators: {
                                notEmpty: {
                                        message: "Aadhar Card is required"
                                }
                            }
                        },
                        certificates: {
                            validators: {
                                notEmpty: {
                                        message: "Provide certificate for vendor"
                                }
                            }
                        }
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger,
                        bootstrap: new FormValidation.plugins.Bootstrap({
                            eleValidClass: ""
                        })
                    }
                }))
        }
    }
}();

jQuery(document).ready((function () {
    KTWizard4.init()
    let avatar5 = new KTImageInput('kt_image_5');
    $("#select2-country").select2({
        placeholder: "Select Country for Vendor",
        allowClear: true,
        width: "100%",
    });
    $("#select2-languages").select2({
        placeholder: "Add languages of Vendor",
        allowClear: true,
        width: "100%",
        tags: true,
    });
    $("#select2-skills").select2({
        placeholder: "Add skills for Vendor",
        allowClear: true,
        width: "100%",
        tags: true,
    });
    $("#select2-rate-type").select2({
        placeholder: "Select Rate Type",
        allowClear: true,
        width: "100%",
        tags: true,
    });
    $("#select2-category").select2({
        placeholder: "Search Category for Vendor",
        allowClear: true,
        width: "100%",
        ajax: {
            url: "/api/v1/admin/vendor-categories-select2/",
            delay: 250,
            data: function (params) {
                var query = {
                    search: params.term
                }
                return query
            },
            processResults: function (data) {
                return data
            },
            cache: true
        }
    });
}
));
