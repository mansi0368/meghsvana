"use strict";
// Class definition
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

 function LoadCategoryTable () {
        var datatable = $('#kt_datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/api/v1/admin/vendor-category-table/',
                        method: 'get',
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // layout definition
            layout: {
                scroll: false,
                footer: false,
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
                input: $('#kt_datatable_search_query'),
                key: 'search'
            },

            // columns definition
            columns: [{
                field: 'id',
                title: '#',
                sortable: 'asc',
                width: 30,
                type: 'number',
                selector: false,
                textAlign: 'center',
            }, {
                field: 'title',
                title: 'TITLE',
            }, {
                field: 'slug',
                title: 'SLUG',
                template: function(row) {
                    return row.slug
                },
            }, {
                field: 'vendors_count',
                title: 'VENDORS',
                type: 'number',
            }],

        });

		$('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();
    }

function AddCategory(event) {
    event.preventDefault();
    let cookie = getCookie('csrftoken');
    let data = new FormData(event.target)
    $.ajax({
        url: '/api/v1/admin/crud-vendor-category/',
        type: 'post',
        headers: { 'X-CSRFToken': cookie },
        data: data,
        processData: false,
        contentType: false,
        beforeSend: function () {
            $('#AddCategoryButton').attr('disabled', true);
            $('#AddCategoryButton').addClass('spinner spinner-white spinner-right')
        },
        success: function (data) {
            Swal.fire({
                position: "center",
                icon: "success",
                title: "Category Created.",
                showConfirmButton: false,
                timer: 1500
            }).then((event) => {
                window.location.reload()
            })
            $('.form-control').val('')
            $('.select2').val('').change()
        },
        error: function (data) {
            if (data['responseJSON']) {
                if (data['responseJSON'].slug) {
                    $('#slug_error').text(data['responseJSON'].slug[0])
                }
                else {
                    $('#slug_error').text('')
                }
            }
            $('#AddCategoryButton').attr('disabled', false);
            $('#AddCategoryButton').removeClass('spinner spinner-white spinner-right')
        },
        complete: function () {
            $('#AddCategoryButton').attr('disabled', false);
            $('#AddCategoryButton').removeClass('spinner spinner-white spinner-right')
        }
    })
}   

jQuery(document).ready(function() {
    LoadCategoryTable();
    $("#select2-parent").select2({
        placeholder: "Search for Parent Category",
        allowClear: true,
        ajax: {
         url: "/api/v1/admin/vendor-categories-select2/",
         delay: 250,
         width: "100%",
         data: function(params) {
            var query = {
                search: params.term
            }
            return query
         },
         processResults: function(data) {
            return data
         },
         cache: true
        }
    });
});
