function getCookie(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie !== '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = cookies[i].trim();
			// Does this cookie string begin with the name we want?
			if (cookie.substring(0, name.length + 1) === (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}


var KTProfile = function () {
	// Elements
	var avatar;
	var offcanvas;

	// Private functions
	var _initAside = function () {
		// Mobile offcanvas for mobile mode
		offcanvas = new KTOffcanvas('vendor_detail_aside', {
            overlay: true,
            baseClass: 'offcanvas-mobile',
            //closeBy: 'kt_user_profile_aside_close',
            toggleBy: 'vendor_detail_aside_toggle'
        });
        console.log(offcanvas)
	}

	var _initForm = function() {
		avatar = new KTImageInput('kt_profile_avatar');
    }
    //console.log(offcanvas)

	return {
		// public functions
		init: function() {
			_initAside();
			_initForm();
		}
	};
}();

$(".modal").on('hidden.bs.modal', function () {
    $('.perm-check').prop('checked', false)
    $('#aa_access').prop('checked', false)
})

"use strict";
// Class definition
var KTDatatableRecordSelectionDemo = function() {
    // Private functions

    var options = {
        // datasource definition
        data: {
            type: 'remote',
            source: {
                read: {
                    url: '/api/v1/admin/vendor-table/',
                    method: 'get',
                },
            },
            pageSize: 1,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
        },
        // layout definition
        layout: {
            scroll: false, // enable/disable datatable scroll both horizontal and
            footer: false // display/hide footer
        },
        // column sorting
        sortable: true,
        pagination: true,
        // columns definition
        columns: [{
            field: 'id',
            title: '#',
            sortable: false,
            width: 20,
            selector: {
                class: ''
            },
            textAlign: 'center',
        },
            {
            field: 'vendor.profile_picture',
            title: 'NAME',
            width: 200,
            textAlign: 'left',
            template: function(data) {
                let user_img = 'background-image:url(\'' + data.vendor.profile_picture + '\')';
                let output = '';
                if (data.vendor.profile_picture) {
                    output = '<div class="d-flex align-items-center">\
                        <div class="symbol symbol-40 flex-shrink-0">\
                            <div class="symbol-label" style="' + user_img + '"></div>\
                        </div>\
                        <div class="ml-2">\
                            <div class="text-dark-75 font-weight-bold line-height-sm">' + data.vendor.full_name + '</div>\
                            <a href="#" class="font-size-sm text-dark-50 text-hover-primary">' 
                            '</a>\
                        </div>\
                    </div>';
                }
                else {
                    var stateNo = KTUtil.getRandomInt(0, 7);
                    var states = [
                        'success',
                        'primary',
                        'danger',
                        'success',
                        'warning',
                        'dark',
                        'primary',
                        'info'];
                    var state = states[stateNo];
                    output = '<div class="d-flex align-items-center">\
                        <div class="symbol symbol-40 symbol-'+state+' flex-shrink-0">\
                            <div class="symbol-label">' + data.vendor.email.substring(0, 1) + '</div>\
                        </div>\
                        <div class="ml-2">\
                            <div class="text-dark-75 font-weight-bold line-height-sm">' + data.vendor.full_name + '</div>\
                            <a href="#" class="font-size-sm text-dark-50 text-hover-primary">' +
                           '</a>\
                        </div>\
                    </div>';
                }

                return output;
            },
        }, {
            field: 'vendor.email',
            title: 'EMAIL',
            
        }, {
            field: 'inr_rate',
            title: 'INR PRICE',
            template: function(row) {
                if (row.inr_rate) {
                   return row.inr_rate;
                } else {
                    return `-`
                }
            },
        }, {
            field: 'usd_rate',
            title: 'USD PRICE',
            template: function(row) {
                if (row.usd_rate) {
                   return row.usd_rate;
                } else {
                    return `-`
                }
            },
        }, {
            field: 'Category',
            title: 'CATEGORY',
            template: function(row) {
                if (row.category) {
                    return row.category
                } else {
                    return `-`
                }
            }
        }, {
            field: 'Status',
            title: 'Status',
            // callback function support for column rendering
            template: function(row) {
                var status = {
                    O: {'title': 'Active', 'class': 'label-light-primary'},
                    F: {'title': 'Offline', 'class': ' label-light-danger'},
                };
                return '<span class="label label-lg font-weight-bold' + status[row.status].class + ' label-inline">' + status[row.status].title + '</span>';
            },
        }, {
            field: 'Actions',
            title: 'Actions',
            sortable: false,
            width: 125,
            overflow: 'visible',
            textAlign: 'left',
	        autoHide: false,
            template: function(row) {
                return `\
                    <div class="dropdown dropdown-inline">\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                            <span class="svg-icon svg-icon-md">\
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                        <rect x="0" y="0" width="24" height="24"/>\
                                        <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                                    </g>\
                                </svg>\
                            </span>\
                        </a>\
                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                            <ul class="navi flex-column navi-hover py-2">\
                                <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">\
                                    Choose an action:\
                                </li>\
                                <li class="navi-item">\
                                    <a href="#" class="navi-link">\
                                        <span class="navi-icon"><i class="la la-check"></i></span>\
                                        <span class="navi-text">Activate</span>\
                                    </a>\
                                </li>\
                                <li class="navi-item">\
                                    <a href="#" class="navi-link">\
                                        <span class="navi-icon"><i class="la la-copy"></i></span>\
                                        <span class="navi-text">Ban</span>\
                                    </a>\
                                </li>\
                                <li class="navi-item">\
                                    <a href="#" class="navi-link">\
                                        <span class="navi-icon"><i class="la la-file-excel-o"></i></span>\
                                        <span class="navi-text">Disable</span>\
                                    </a>\
                                </li>\
                            </ul>\
                        </div>\
                    </div>\
                    <a id="vendor_detail_aside_toggle" href="javascript:;" onclick="LoadSideBar(${row.id})" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit details">\
                        <span class="svg-icon svg-icon-md">\
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                    <rect x="0" y="0" width="24" height="24"/>\
                                    <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero"\ transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "/>\
                                    <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"/>\
                                </g>\
                            </svg>\
                        </span>\
                    </a>\
                    <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete">\
                        <span class="svg-icon svg-icon-md">\
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                    <rect x="0" y="0" width="24" height="24"/>\
                                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>\
                                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>\
                                </g>\
                            </svg>\
                        </span>\
                    </a>\
                `;
            },
        }],
    };

    // basic demo
    var localSelectorDemo = function() {
        // enable extension
        options.extensions = {
            // boolean or object (extension options)
            checkbox: true,
        };

        options.search = {
            input: $('#kt_datatable_search_query'),
            key: 'search'
        };

        var datatable = $('#kt_datatable').KTDatatable(options);

        $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_status').selectpicker();

        datatable.on(
            'datatable-on-check datatable-on-uncheck',
            function(e) {
                var checkedNodes = datatable.rows('.datatable-row-active').nodes();
                var count = checkedNodes.length;
                $('#kt_datatable_selected_records').html(count + '');
                if (count > 0) {
                    $('#kt_datatable_group_action_form').collapse('show');
                } else {
                    $('#kt_datatable_group_action_form').collapse('hide');
                }
            });

        $('#kt_datatable_fetch_modal').on('show.bs.modal', function(e) {
            var ids = datatable.rows('.datatable-row-active').
            nodes().
            find('.checkbox > [type="checkbox"]').
            map(function(i, chk) {
                return $(chk).val();
            });
            console.log(ids);
            var c = document.createDocumentFragment();
            for (var i = 0; i < ids.length; i++) {
                var li = document.createElement('li');
                li.setAttribute('data-id', ids[i]);
                li.innerHTML = 'Selected record ID: ' + ids[i];
                c.appendChild(li);
            }
            $('#kt_datatable_fetch_display').append(c);
        }).on('hide.bs.modal', function(e) {
            $('#kt_datatable_fetch_display').empty();
        });
    };

    var serverSelectorDemo = function() {
        // enable extension
        options.extensions = {
            // boolean or object (extension options)
            checkbox: true,
        };
        options.search = {
            input: $('#kt_datatable_search_query_2'),
            key: 'generalSearch'
        };

        var datatable = $('#kt_datatable_2').KTDatatable(options);

        $('#kt_datatable_search_status_2').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type_2').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status_2, #kt_datatable_search_type_2').selectpicker();

        datatable.on(
            'datatable-on-click-checkbox',
            function(e) {
                // datatable.checkbox() access to extension methods
                var ids = datatable.checkbox().getSelectedId();
                var count = ids.length;

                $('#kt_datatable_selected_records_2').html(count);

                if (count > 0) {
                    $('#kt_datatable_group_action_form_2').collapse('show');
                } else {
                    $('#kt_datatable_group_action_form_2').collapse('hide');
                }
            });

        $('#kt_datatable_fetch_modal_2').on('show.bs.modal', function(e) {
            var ids = datatable.checkbox().getSelectedId();
            var c = document.createDocumentFragment();
            for (var i = 0; i < ids.length; i++) {
                var li = document.createElement('li');
                li.setAttribute('data-id', ids[i]);
                li.innerHTML = 'Selected record ID: ' + ids[i];
                c.appendChild(li);
            }
            $('#kt_datatable_fetch_display_2').append(c);
        }).on('hide.bs.modal', function(e) {
            $('#kt_datatable_fetch_display_2').empty();
        });
    };
    return {
        // public functions
        init: function() {
            localSelectorDemo();
            //serverSelectorDemo();
        },
    };
    
}();

function LoadSkills(skills, id) {
    let i;
    let skill_string = ''
    if (skills) {
    for (i=0; i <= skills.length - 1 ; i++) {
           skill_string += `<div class="col-2">
           <a class="btn btn-sm btn-light-primary mx-2 font-weight-bolder py-2 px-5">${skills[i]}</a>
           </div> ` + ` `
        }
    } 
    else {
        skill_string = `No skills present for Vendor`
    }
    return skill_string
}

function LoadReviews(reviews, id) { 
    let review_string = ''
    if (reviews.length == 0) {
        return `<div class="d-flex align-items-center bg-light-warning rounded p-5 gutter-b">
        <div class="d-flex flex-column flex-grow-1 mr-2">
            <a href="#" class="font-weight-normal text-dark-75 text-hover-primary font-size-lg mb-1">No Review Added</a>
        </div>
        </div>`
    } else {
        let i;
        for (i=0; i<=reviews.length -1 ; i++) {
            review_string += `<div class="d-flex align-items-center bg-light-warning rounded p-5 gutter-b">
            <div class="symbol symbol-40 symbol-circle mr-3 mb-3" data-toggle="tooltip" title="" data-original-title="${reviews[i].created_by.full_name}">
                <img alt="nav-item" src="${reviews[i].created_by.profile_picture}">
            </div>
            <div class="d-flex flex-column flex-grow-1 mr-2">
                <a href="#" class="font-weight-normal text-dark-75 text-hover-primary font-size-lg mb-1">${reviews[i].created_by.full_name} <span class="font-weight-bolder text-warning py-1 font-size-lg">28%</span></a>
                <span class="text-muted font-size-sm">${reviews[i].review.slice(0, 50)}</span>
            </div>
        </div> `
        }
    }
    return review_string
}

function LoadLocalPermissions(data) {
    let permissions = '';
    if (data.is_vc == true) {
        permissions += 'Video Call,'
    } 
    if (data.is_ch == true) {
        permissions += 'Chat,'
    } 
    if (data.is_ls == true) {
        permissions +=  'Live Service,'
    } 
    if (data.is_tr == true) {
        permissions += 'Text Report,'
    } 
    if (data.is_vr == true) {
        permissions += 'Video Report'
    }
    if (permissions.length == 0) {
        return `None`
    } else {
        return permissions
    }
}

function LoadGlobalPermissions(data) {
    let permissions = '';
    if (data.is_vc == true) {
        permissions += 'Video Call,'
    }
    if (data.is_ch == true) {
        permissions += 'Chat,'
    } 
    if (data.is_ls == true) {
        permissions +=  'Live Service,'
    }
    if (data.is_tr == true) {
        permissions += 'Text Report,'
    } 
    if (data.is_vr == true) {
        permissions += 'Video Report'
    }
    if (permissions.length == 0) {
        return `None`
    } else {
        return permissions
    }
}

function CheckOffOnlineStatus(data) {
    if (data == 'ON') {
        return `<i class="symbol-badge bg-success"></i>`
    } else {
        return `<i class="symbol-badge bg-danger"></i>`
    }
}

function LoadPermissionModal(ID) {
    $.ajax({
        url: `/api/v1/admin/rud-vendor-profile/${ID}/`,
        type: 'get',
        success: function (data) {
            if (data.is_vc == true) {
              $('#local-check-vc').prop('checked', true)
            }
            if (data.is_ac == true) {
              $('#local-check-ac').prop('checked', true)
            }
            if (data.is_ch == true) {
              $('#local-check-ch').prop('checked', true)
            } 
            if (data.is_ls == true) {
              $('#local-check-ls').prop('checked', true)
            }
            if (data.is_tr == true) {
              $('#local-check-tr').prop('checked', true) 
            } 
            if (data.global_permissions.is_vr == true) {
              $('#local-check-vr').prop('checked', true)
            }
            if (data.global_permissions.is_vc == true) {
                $('#global-check-vc').prop('checked', true)
            }
            if (data.global_permissions.is_ac == true) {
                $('#global-check-ac').prop('checked', true)
            }
            if (data.global_permissions.is_ch == true) {
                $('#global-check-ch').prop('checked', true)
            } 
            if (data.global_permissions.is_ls == true) {
                $('#global-check-ls').prop('checked', true)
            }
            if (data.global_permissions.is_tr == true) {
                $('#global-check-tr').prop('checked', true)
            } 
            if (data.global_permissions.is_vr == true) {
                $('#global-check-vr').prop('checked', true)
            }
            if (data.aa_access == true) {
                $('#aa_access').prop('checked', true)
            }
            else {
                $('#aa_access').prop('checked', false)
            }
        },
        error: function (data) {
            Swal.fire({
                icon: "error",
                title: data['statusText'],
                showConfirmButton: false,
                timer: 1500
            });
        },
        complete: function () {
            $('#PermissionsModal').modal('show')
            $('#PermissionsModal').attr('data-id', ID)
        }
    })
}

function UpdatePermissions(event) {
    event.preventDefault()
	let data = new FormData(event.target)
	let cookie = getCookie('csrftoken');
    let ID = $('#PermissionsModal').attr('data-id')
    $.ajax({
        url: `/api/v1/admin/rud-vendor-profile/${ID}/`,
        type: 'patch',
        processData: false,
		contentType: false,
		headers: { 'X-CSRFToken': cookie },
        data: data,
        beforeSend: function () {
            $('#ConfirmButton').attr('disabled', true);
            $('#ConfirmButton').addClass('spinner spinner-white spinner-right')
        },
        success: function (data) {
            Swal.fire({
				icon: "success",
				title: "Vendor Created Succesfully",
				showConfirmButton: false,
				timer: 1500
			})
            $('#PermissionsModal').modal('hide')
        },
        error: function (data) {
            Swal.fire({
                icon: "error",
                title: data['statusText'],
                showConfirmButton: false,
                timer: 1500
            });
            $('#ConfirmButton').attr('disabled', true);
            $('#ConfirmButton').addClass('spinner spinner-white spinner-right')
        },
        complete: function () {
            $('#ConfirmButton').attr('disabled', false);
            $('#ConfirmButton').removeClass('spinner spinner-white spinner-right')
        }
    })
}

function VendorSideBarContent(data) {
    return `
    <!--begin::Header-->
    <div class="d-flex align-items-center mt-5">
        <div class="symbol symbol-100 mr-5">
            <div class="symbol-label" style="background-image:url('${data.vendor_user.profile_picture}')"></div>
            ${CheckOffOnlineStatus(data.status)}
        </div>
        <div class="d-flex flex-column">
            <a href="javascript:;" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">${data.vendor_user.full_name}</a>
            <div class="navi mt-2">
                <a href="javascript:;" class="navi-item">
                    <span class="navi-link p-0 pb-2">
                        <span class="navi-icon mr-1">
                            <span class="svg-icon svg-icon-lg svg-icon-primary">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-notification.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000" />
                                        <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5" />
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                        </span>
                        <span class="navi-text text-muted text-hover-primary">${data.vendor_user.email}</span>
                    </span>
                </a>
                <a href="javascript:;" class="btn btn-sm btn-light-primary font-weight-bolder py-2 px-5">${data.rating_point}</a>
            </div>
        </div>
    </div>
    <!--end::Header-->
    <!--begin::Separator-->
    <div class="separator separator-dashed mt-8 mb-5"></div>
    <!--end::Separator-->
    <!--begin::Nav-->
    <div class="navi navi-spacer-x-0 p-0">
        <h5 class="font-weight-bold m-0 mb-5">Performance
        </h5>
        <div class="d-flex align-items-center flex-wrap">
            <!--begin: Item-->
            <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                <span class="mr-4">
                    <i class="flaticon-piggy-bank icon-2x text-muted font-weight-bold"></i>
                </span>
                <div class="d-flex flex-column text-dark-75">
                    <span class="font-weight-bolder font-size-sm">Revenue</span>
                    <span class="font-weight-bolder font-size-h5">
                    ${data.revenue}</span>
                </div>
            </div>
            <!--end: Item-->
            <!--begin: Item-->
            <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                <span class="ml-10 mr-4">
                    <i class="flaticon-confetti icon-2x text-muted font-weight-bold"></i>
                </span>
                <div class="d-flex flex-column text-dark-75">
                    <span class="font-weight-bolder font-size-sm">Orders</span>
                    <span class="font-weight-bolder font-size-h5">
                    ${data.order_count}</span>
                </div>
            </div>
            <!--end: Item-->
            <!--begin: Item-->
            <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                <span class="ml-auto mr-4">
                    <i class="flaticon-pie-chart icon-2x text-muted font-weight-bold"></i>
                </span>
                <div class="d-flex flex-column text-dark-75">
                    <span class="font-weight-bolder font-size-sm">CTR</span>
                    <span class="font-weight-bolder font-size-h5">
                    22.5<span class="text-dark-50 font-weight-bold">%</span></span>
                </div>
            </div>
            <!--end: Item-->
            <!--begin: Item-->
        </div>
        <h5 class="font-weight-bold m-0 mb-5 mt-5">BIO
        </h5>
        <div class="d-flex align-items-center flex-wrap">
            <p>${data.vendor_user.about}</p>
        </div>
        <h5 class="font-weight-bold m-0 mb-5 mt-5">SKILLS
            </h5>
            <div class="d-flex align-items-center flex-wrap">
                <div id="skills-${data.vendor_user.id}" class="row">
                    ${  
                        LoadSkills(data.skills, data.vendor_user.id)
                    }
            </div>
        </div>
        <div class="row ml-1">
        <h5 class="font-weight-bold m-0 mt-5">Permissions
        </h5>
        <a href="javascript:;" onclick="LoadPermissionModal(${data.id})" class="navi-item ml-auto mt-5 mr-4">change</a>
        </div>
        <span class="navi-text text-muted ml-1">Set access for vendor's services here</span>
        <!--begin::Item-->
        <a href="javascript:;" class="navi-item">
            <div class="navi-link">
                <div class="symbol symbol-40 mr-3">
                    <div class="symbol-label">
                        <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-02-01-052524/theme/html/demo5/dist/../src/media/svg/icons/Cooking/Dish.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M12,21 C7.02943725,21 3,16.9705627 3,12 C3,7.02943725 7.02943725,3 12,3 C16.9705627,3 21,7.02943725 21,12 C21,16.9705627 16.9705627,21 12,21 Z M12,18 C15.3137085,18 18,15.3137085 18,12 C18,8.6862915 15.3137085,6 12,6 C8.6862915,6 6,8.6862915 6,12 C6,15.3137085 8.6862915,18 12,18 Z" fill="#000000"/>
                                <path d="M12,16 C14.209139,16 16,14.209139 16,12 C16,9.790861 14.209139,8 12,8 C9.790861,8 8,9.790861 8,12 C8,14.209139 9.790861,16 12,16 Z" fill="#000000" opacity="0.3"/>
                            </g>
                        </svg><!--end::Svg Icon--></span>
                    </div>
                </div>
                <div class="navi-text">
                    <div class="font-weight-bold">Local Permissions: ${LoadLocalPermissions(data)}</div>
                </div>
            </div>
        </a>
        <!--end:Item-->
         <!--begin::Item-->
         <a href="javascript:;" class="navi-item">
            <div class="navi-link">
                <div class="symbol symbol-40 mr-3">
                    <div class="symbol-label">
                        <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-02-01-052524/theme/html/demo5/dist/../src/media/svg/icons/Cooking/Dish.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M12,21 C7.02943725,21 3,16.9705627 3,12 C3,7.02943725 7.02943725,3 12,3 C16.9705627,3 21,7.02943725 21,12 C21,16.9705627 16.9705627,21 12,21 Z M12,18 C15.3137085,18 18,15.3137085 18,12 C18,8.6862915 15.3137085,6 12,6 C8.6862915,6 6,8.6862915 6,12 C6,15.3137085 8.6862915,18 12,18 Z" fill="#000000"/>
                                <path d="M12,16 C14.209139,16 16,14.209139 16,12 C16,9.790861 14.209139,8 12,8 C9.790861,8 8,9.790861 8,12 C8,14.209139 9.790861,16 12,16 Z" fill="#000000" opacity="0.3"/>
                            </g>
                        </svg><!--end::Svg Icon--></span>
                    </div>
                </div>
                <div class="navi-text">
                    <div class="font-weight-bold">Global Permissions: ${LoadGlobalPermissions(data.global_permissions)}</div>
                </div>
            </div>
        </a>
        <!--end:Item-->
         <!--begin::Item-->
         <a href="javascript:;" class="navi-item">
            <div class="navi-link">
                <div class="symbol symbol-40 mr-3">
                    <div class="symbol-label">
                        <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-02-01-052524/theme/html/demo5/dist/../src/media/svg/icons/Cooking/Dish.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M12,21 C7.02943725,21 3,16.9705627 3,12 C3,7.02943725 7.02943725,3 12,3 C16.9705627,3 21,7.02943725 21,12 C21,16.9705627 16.9705627,21 12,21 Z M12,18 C15.3137085,18 18,15.3137085 18,12 C18,8.6862915 15.3137085,6 12,6 C8.6862915,6 6,8.6862915 6,12 C6,15.3137085 8.6862915,18 12,18 Z" fill="#000000"/>
                                <path d="M12,16 C14.209139,16 16,14.209139 16,12 C16,9.790861 14.209139,8 12,8 C9.790861,8 8,9.790861 8,12 C8,14.209139 9.790861,16 12,16 Z" fill="#000000" opacity="0.3"/>
                            </g>
                        </svg><!--end::Svg Icon--></span>
                    </div>
                </div>
                <div class="navi-text">
                    <div class="font-weight-bold">AA Access: ${data.aa_access_marker}</div>
                </div>
            </div>
        </a>
        <!--end:Item-->
    </div>
    <!--end::Nav-->
    <!--begin::Separator-->
    <div class="separator separator-dashed my-7"></div>
    <!--end::Separator-->
    <!--begin::Notifications-->
    <div>
        <!--begin:Heading-->
        <h5 class="mb-5">Recent Reviews</h5>
        <!--end:Heading-->
        <!--begin::Item-->
        <div id="reviews-${data.vendor_user.id}" class="row">
        ${LoadReviews(data.reviews, data.vendor_user.id)}
        </div>
        <!--end::Item-->
    </div>
    <!--end::Notifications-->
    `
}

function LoadSideBar(ID) {
    $.ajax({
        url: `/api/v1/admin/rud-vendor-profile/${ID}/`,
        type: 'get',
        beforeSend: function () {
            $('#sidebar-content').html(`<div class="spinner spinner-primary spinner-lg mr-15"></div>`)
        },
        success: function (data) {
            $('#sidebar-content').empty()
            let template = VendorSideBarContent(data)
            $('#sidebar-content').append(template)
        },
        error: function (data) {
            Swal.fire({
                icon: "error",
                title: data['statusText'],
                showConfirmButton: false,
                timer: 1500
            });
        },
        complete: function () {
            $('#vendor_detail_aside').addClass('offcanvas-on wd-700')
        }
    })
}

$('#kt_quick_user_close').on('click', function() {
    $('#vendor_detail_aside').removeClass('offcanvas-on wd-700')
})

jQuery(document).ready(function() {
    KTDatatableRecordSelectionDemo.init();
    KTProfile.init();
});