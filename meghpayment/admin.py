from django.contrib import admin
from meghcustomer.models import Order
from meghauth.models import MeghSvanaUserCoinsModel
from meghpayment.models import Transaction

class TransactionAdmin(admin.ModelAdmin):

    model = Transaction
    fields = ('tid','order','wallet','amount', 'type', 'created_at', 'updated_at')
    readonly_fields = ('created_at', 'updated_at')
    list_display = ('tid','order','wallet','amount', 'type',  'created_at', 'updated_at')    


# Register your models here.
admin.site.register(Transaction, TransactionAdmin)
