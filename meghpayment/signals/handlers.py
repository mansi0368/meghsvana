# -*- coding: utf-8 -*-
from meghcustomer.models import Order
from meghauth.models import MeghSvanaUserCoinsModel
from meghpayment.models import Transaction
from django.db.models.signals import pre_save


def post_save_coinsmodel(sender, instance, created, *args, **kwargs):
    if created:
         instance.transaction = Transaction.objects.create(amount=instance.amount, created_by=instance.customer)
         instance.save()
    else:
        pass

