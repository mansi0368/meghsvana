from django.db import models
from meghcustomer.models import Order
from meghauth.models import MeghSvanaUserCoinsModel
import uuid

# Create your models here.
class Transaction(models.Model):
    TYPE_CHOICE = (
        ('I', 'In'),
        ('O', 'Out'),
        ) 
    tid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='orders')
    wallet = models.ForeignKey(MeghSvanaUserCoinsModel, on_delete=models.CASCADE, related_name='transaction')
    amount = models.DecimalField(max_digits=15, decimal_places=2)
    type = models.CharField(choices= TYPE_CHOICE, max_length=70)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
class Meta:
        verbose_name = 'Transaction'
        verbose_name_plural = 'Transactions'