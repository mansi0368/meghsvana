from django.apps import AppConfig


class MeghpaymentConfig(AppConfig):
    name = 'meghpayment'
