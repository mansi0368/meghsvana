from django.apps import AppConfig
from django.apps import AppConfig
from django.db.models.signals import post_save


class MeghchatserverConfig(AppConfig):
    name = 'meghchatserver'
def ready(self):
    from meghchatserver.models import CustomerChatRequest
    from meghauth.models import CustomerUser
    from meghchatserver.signals.handlers import post_save_chat_request

    post_save.connect(post_save_chat_request, sender=CustomerUser)