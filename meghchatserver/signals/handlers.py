# -*- coding: utf-8 -*-
from meghcustomer.models import Order
from meghchatserver.models import CustomerChatRequest

def post_save_chat_message(sender, instance, created, *args, **kwargs):
    if created and instance.is_viewed is False:
        """
        Call a task of celery and give the instance to the task
        it will send it to web socket into the group which is the thread id.
        But check for not viewed then only send these messges to websocket.
        """
        pass
    else:
        pass

def post_save_chat_request(sender, instance, created, *args, **kwargs):
    if created:
        Order.objects.create(customer=instance.customer, created_by=instance.customer, services='CH', status='INI')
    else:
        pass

   

