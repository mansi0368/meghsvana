# -*- coding: utf-8 -*-

from django.shortcuts import (render, redirect)
from django.views.generic import View
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin
from meghchatserver.models import ChatThread, ChatMessage
from django.http import Http404

class ChatThreadView(LoginRequiredMixin, View):

    template_name = 'chat.html'

    def get(self, request):
        threads = ChatThread.objects.all()
        return render(request, self.template_name, context={'threads': threads}, status=200)


class ChatWindowView(LoginRequiredMixin, View):
    
    template_name = 'window.html'

    def get(self, request, token):
        try:
            obj = ChatThread.objects.filter(Q(p_one=request.user) | Q(p_two=request.user)).get(token=token)
            return render(request, self.template_name, context={'thread': obj}, status=200)
        except ChatThread.DoesNotExist:
            raise Http404("Resource Does Not exist on server")
