# -*- coding: utf-8 -*-

from django.contrib import admin
from meghchatserver.models import (ChatThread, ChatMessage, CustomerChatRequest)


class ChatThreadAdmin(admin.ModelAdmin):

    model = ChatThread
    fields = ('token', 'p_one', 'p_two', 'p_one_status', 'p_two_status', 'is_active', 'duration', 'created_at', 'updated_at')
    readonly_fields = ('token', 'duration', 'created_at', 'updated_at')
    list_filter = ('p_one_status', 'p_two_status')
    list_display = ('token', 'p_one', 'p_two', 'p_one_status', 'p_two_status', 'is_active', 'duration', 'created_at', 'updated_at')


class ChatMessageAdmin(admin.ModelAdmin):

    model = ChatMessage
    fields = ('thread', 'text', 'sender', 'reciever', 'is_viewed', 'created_at', 'updated_at')
    readonly_fields = ('created_at', 'updated_at')
    list_filter = ('is_viewed',)
    list_display = ('thread', 'text', 'sender', 'reciever', 'is_viewed', 'created_at', 'updated_at')


class CustomerChatRequestAdmin(admin.ModelAdmin):

    model = ChatMessage
    fields = ('status','customer','vendor','chat_thread', 'created_at', 'updated_at')
    readonly_fields = ('created_at', 'updated_at')
    list_display = ('status','customer','vendor','chat_thread', 'created_at', 'updated_at')    


# Register your models here.
admin.site.register(ChatThread, ChatThreadAdmin)
admin.site.register(ChatMessage, ChatMessageAdmin)
admin.site.register(CustomerChatRequest,  CustomerChatRequestAdmin)
