# -*- coding: utf-8 -*-

import json
from django.db.models import Q
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.exceptions import DenyConnection
from meghchatserver.models import (ChatThread, ChatMessage)
from meghauth.models import (MeghSvanaUser,)
from meghchatserver.models import ChatThread
from channels.db import database_sync_to_async

class MeghSvanaChatConsumer(AsyncWebsocketConsumer):
    """
    Consumer to handle chat messages on websocket protocol
    for seamless comm/
    """
    async def connect(self):
        self.token = self.scope['url_route']['kwargs']['token']
        self.user = self.scope["user"].id
        print("User == {user}".format(user=self.user))
        print("Token == {token}".format(token=self.token))
        
        self.group = 'chat_{token}'.format(token=self.token)
        print("Group Name == {group} || Channel Name == {channel}".format(group=self.group,
                                                                          channel=self.channel_name))
        await self.check_token_and_user(self.token, self.user)
        await self.channel_layer.group_add(
            self.group,
            self.channel_name
        )
        await self.accept()

    
    async def disconnect(self, close_code):
        print("Close Code == {code}".format(code=close_code))
        await self.channel_layer.group_discard(
            self.group,
            self.channel_name
        )

    async def receive(self, text_data):
        json_data = json.loads(text_data)
        message = json_data['message']
        print("Messaging User - {user}".format(user=self.user))
        print("Message {message}".format(message=message))
        print("Messaging thread {token}".format(token=self.token))
        """
        Add a celery task here to load data to server 
        database with is viewed set to true
        
        """
        await self.channel_layer.group_send(
            self.group,
            {
                'type': 'chat_message',
                'message': message,
                'sender': self.user
            }
        )
    
    async def chat_message(self, event):
        message = event['message']
        sender = event['sender']
        await self.send(text_data=json.dumps({
            'message': message,
            'sender': str(self.scope["user"].profile_picture)
        }))
    
    @database_sync_to_async
    def check_token_and_user(self, token, user):
        try:
            ChatThread.objects.filter(Q(p_one_id=user) | Q(p_two_id=user)).get(token=token)
        except ChatThread.DoesNotExist:
            raise DenyConnection
