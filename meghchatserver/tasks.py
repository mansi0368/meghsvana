# -*- coding: utf-8 -*-

from celery import shared_task


@shared_task
def _close_chat(token):
    """
    Task to discard the group from channel layer
    i.e discconect the Sockets
    """
    return 0
