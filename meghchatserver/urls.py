# -*- coding: utf-8 -*-

from meghchatserver import consumers
from django.conf.urls import re_path
from django.urls import path
from meghchatserver import views

websocket_urlpatterns = [
    re_path(r'ws/chat/(?P<token>[0-9A-Fa-f-]+)/$', consumers.MeghSvanaChatConsumer.as_asgi()),
]

urlpatterns = [
    path('chats/', views.ChatThreadView.as_view(), name='chats'),
    path('chat/<str:token>/', views.ChatWindowView.as_view(), name='chat')
]
