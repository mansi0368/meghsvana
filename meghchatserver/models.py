# -*- coding: utf-8 -*-

import uuid
from django.db import (models, transaction)
from django.db.models import Q
from meghauth.models import MeghSvanaUser
from django.core.exceptions import ValidationError
from meghcustomer.models import Order

# Create your models here.

class ChatThread(models.Model):
    
    token = models.UUIDField(default=uuid.uuid4, editable=False)
    p_one = models.ForeignKey(MeghSvanaUser, on_delete=models.CASCADE, related_name='p_one_chatthreads')
    p_two = models.ForeignKey(MeghSvanaUser, on_delete=models.CASCADE, related_name='p_two_chatthreads')
    p_one_status = models.BooleanField(default=True)
    p_two_status = models.BooleanField(default=True)
    is_active = models.BooleanField()
    duration = models.DurationField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '#ID-%s - %s - %s' % (self.token, self.p_one, self.p_two)
    
    class Meta:
        verbose_name = 'Chat Thread'
        verbose_name_plural = 'Chat Threads'


class ChatMessage(models.Model):
    
    text = models.TextField()
    thread = models.ForeignKey(ChatThread, on_delete=models.CASCADE, related_name='chat_messages')
    #attachment = models.FileField(null=True, blank=True)
    sender = models.ForeignKey(MeghSvanaUser, on_delete=models.CASCADE, related_name='sent_messages')
    reciever = models.ForeignKey(MeghSvanaUser, on_delete=models.CASCADE, related_name='recieved_messages')
    is_viewed = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '#ID-%s - %s - %s' % (self.id, self.sender, self.reciever) 

    # def clean(self):
    #     if self.text is None and self.attachement is None:
    #         raise ValidationError({"text": "Atleast Chat message or an attachment is required.",
    #                                "attachemnt": "Atleast Chat message or an attachment is required."})
    #     else:
    #         pass
    
    def length(self):
        if self.text:
            return len(self.text)
        else:
            return None

    def thread_message_counter(self):
        return self.thread.chat_messages.count()

    class Meta:
        verbose_name = 'Chat Message'
        verbose_name_plural = 'Chat Messages' 
        ordering = ('-created_at',)


class CustomerChatRequest(models.Model):
    STATUS_CHOICE = (
        ('C', 'Completed'),
        ('R', 'Refunded'),
        ('Q', 'Queued')
    ) 
    status = models.CharField(choices=STATUS_CHOICE, max_length=50, default='Q')
    order = models.OneToOneField(Order, on_delete=models.CASCADE, null=True, blank=True, related_name='customerchatrequest')
    customer = models.ForeignKey(MeghSvanaUser, on_delete=models.CASCADE, related_name='customer_customerchatrequests')
    vendor = models.ForeignKey(MeghSvanaUser, on_delete=models.CASCADE, related_name='vendor_customerchatrequests')
    chat_thread = models.ForeignKey(ChatThread, on_delete=models.CASCADE, related_name='customerchatrequests', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        with transaction.atomic():
            self.chat_thread = ChatThread.objects.get(Q(Q(p_one=self.customer)|Q(p_two=self.vendor))|Q(Q(p_one=self.vendor)|Q(p_two=self.customer)))
            super(CustomerChatRequest, self).save(*args, **kwargs)
    
    class Meta:
        verbose_name = 'Customer Chat Request'
        verbose_name_plural = 'Customer Chat Requests'

    
