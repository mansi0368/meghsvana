FROM ubuntu:18.04

RUN apt-get update && apt-get install --no-install-recommends -y python3.8 python3-pip python3.8-dev libpq-dev gcc musl-dev python3-dev libffi-dev cargo libjpeg-dev zlib1g-dev libssl-dev netcat
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN ln -s /usr/bin/python3.8 /usr/bin/python
RUN python -m pip install --upgrade pip
RUN python -m pip install setuptools
WORKDIR /usr/src/meghsvana
ENV APP_HOME=/usr/src/meghsvana

COPY . $APP_HOME

RUN python -m pip install -r requirements.txt --no-cache-dir

RUN mkdir -p $APP_HOME
RUN mkdir -p $APP_HOME/staticfiles
RUN mkdir -p $APP_HOME/media

# copy entrypoint-prod.sh
COPY ./entrypoint.sh $APP_HOME
COPY ./wait-for /bin/wait-for

# run entrypoint.sh
ENTRYPOINT ["/usr/src/meghsvana/entrypoint.sh"]
